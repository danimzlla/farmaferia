<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
    include_once("modelo/Orm.php");

    if(isset($_POST['bti'])){
      $u = $_POST['cor'];
      $p = $_POST['pas'];

      if(strlen($u) <= 4){
        echo "<script>window.location = '?err=2&usu';</script>";
        exit(1);
      }

      if(strlen($p) < 5){
        echo "<script>window.location = '?err=3&pas';</script>";
        exit(1);
      }

      $rl = $orm->iniciarSesionAdmin($u, $p);
      if($rl == false){
        echo "<script>window.location = '?log=0&err=1';</script>";
        session_destroy();
        exit(1);
      }else{
        session_start();
        $_SESSION['login'] = true;
        $_SESSION['idu'] = $rl['id'];
        $_SESSION['usuario'] = $u;
        $_SESSION['acceso'] = $rl['acceso'];
        echo "<script>window.location = 'index.php';</script>";
        exit(1);
      }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<title>Panel administrador Automotores Guatire</title>

	<link href="../static/img/favicon.png" rel="icon">
	<link href="../static/img/apple-touch-icon.png" rel="apple-touch-icon">

	<link href="../static/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="../static/css/panel.css" rel="stylesheet" type="text/css">
	<link href="../static/css/style.css" rel="stylesheet" type="text/css">
</head>


<body class="bg-gradient-primary">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-12 col-md-9">
				<div class="card o-hidden border-0 shadow-lg my-5">
					<div class="card-body p-0">
						<div class="row">
							<div class="col-lg-12">
								<div class="p-5">
									<form class="form-a" method="POST" action="">
										<div class="row">
											<div class="col-md-12 mb-2">
												<div class="title-box-d">
													<h4 class="title-d">Iniciar sesión, Panel administrativo de Automotores Guatire</h4>
												</div>
											</div>

											<div class="col-md-12 mb-2">
												<div class="form-group">
													<label for="correo">Ingresa tu correo electrónico</label>
													<input type="text" name="cor" class="form-control form-control-lg form-control-a" placeholder="">
												</div>
											</div>

											<div class="col-md-12 mb-2">
												<div class="form-group">
													<label for="Contraseña">Ingresa tu contraseña</label>
													<input type="password" name="pas" class="form-control form-control-lg form-control-a" placeholder="">
												</div>
											</div>

											<div class="col-md-12">

                                                <?php 

                                                    if(isset($_GET['err'])){
                                                      $err = $_GET['err'];
                                                      if($err == 2)
                                                        $msj = "El usuario debe tener más de 4 caracteres";
                                                      else if($err == 3)
                                                        $msj = "La contraseña debe tener mínimo 5 caracteres";
                                                      else if($err == 1)
                                                        $msj = "Los datos de acceso son incorrectos.";
                                                ?>

                                                <h5 style="color:red;"><?php echo $msj;?></h5>
                                                <br><br>

                                                <?php } ?>
                                                
												<button type="submit" class="btn btn-primary" name="bti">Ingresar</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="../static/js/jquery.min.js"></script>
	<script src="../static/js/bootstrap.bundle.min.js"></script>
	<script src="../static/js/jquery.easing.min.js"></script>
	<script src="../static/js/sb-admin-2.min.js"></script>
</body>
</html>
