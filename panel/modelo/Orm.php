<?php
	date_default_timezone_set('America/Caracas');
	error_reporting(0);
	include_once("Conexion.php");
	require_once('PHPMailer/class.phpmailer.php');
    require_once('PHPMailer/class.smtp.php');

    class Orm{
		private $mysql = null;

		public function Orm($mysql){
			$this->mysql = $mysql;
		}

        public function obtenerDominio(){
          return $this->mysql->dominio;
        }

        public function userSms(){
          return $this->mysql->usersms;
        }

        public function pasSms(){
          return $this->mysql->passms;
        }

		public function urls($url) {

		  // Tranformamos todo a minusculas

		  $url = strtolower($url);

		  //Rememplazamos caracteres especiales latinos

		  $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');

		  $repl = array('a', 'e', 'i', 'o', 'u', 'n');

		  $url = str_replace ($find, $repl, $url);

		  // Añadimos los guiones

		  $find = array(' ', '&', '\r\n', '\n', '+');
		  $url = str_replace ($find, '-', $url);

		  // Eliminamos y Reemplazamos otros carácteres especiales

		  $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');

		  $repl = array('', '-', '');

		  $url = preg_replace ($find, $repl, $url);

		  return $url;

		}

        public function monto($m){
            //return number_format(round($m, 2));
            return number_format($m, 2, ',', '.');
        }
        public function insertarPersonalizado($sql){
			$resp = $this->mysql->ejecutar($sql);
			if($resp->errno){
				return false;
			}else{
				return $resp;
			}
        }
		public function insertar($datos, $tabla){
			$valores = $this->unir(",", $datos);
		    $sql = "INSERT INTO $tabla VALUES($valores);";
			$resp = $this->mysql->ejecutar($sql); 
			if($resp->errno){
				return false;
			}else{
				return $resp;
			}
		}
		public function consultaCondicion($campo, $valor, $tabla){
            $valor = $this->mysql->con->real_escape_string($valor);
            $sql = "SELECT * FROM $tabla WHERE $campo='$valor'";
			$resp = $this->mysql->consultar($sql);
			return $resp;
		}

		public function consultaGeneral($tabla){
			$resp = $this->mysql->consultar("SELECT * FROM $tabla");
			return $resp;
		}
        public function ultimoCambioDeClave($usuario){
            $f = date('Y-m-d');
            $sql = "SELECT DATEDIFF('$f', fecha_cambio) n FROM usuario;";
			$resp = $this->mysql->consultar($sql);
            $x = $resp->fetch_array();
            return $x[0];
        }


        public function iniciarSesionAdmin($u, $p){
            $u = $this->mysql->con->real_escape_string($u);
            $p = $this->mysql->con->real_escape_string($p);
            $sql = "SELECT * FROM admins where correo='$u' AND password=md5('$p');";
            $r = $this->mysql->consultar($sql);
            if($f = $r->fetch_assoc()){
                return $f;
            }else{
                return false;
            }
        }
        public function consultaPersonalizada($sql){
			$resp = $this->mysql->consultar($sql);
			return $resp;
        }
		public function eliminar($campo, $valor, $tabla){
            $valor = $this->mysql->con->real_escape_string($valor);
		    $sql = "DELETE FROM $tabla WHERE $campo='$valor'";
			$resp = $this->mysql->ejecutar($sql);
			if($resp->affected_rows<1)
				return false;
			else
				return true;
		}
        public function eliminarPersonalizado($sql){
			$resp = $this->mysql->ejecutar($sql);
			if($resp->affected_rows<1)
				return false;
			else
				return true;
        }
		public function editar($cabeceras, $nuevos, $campo, $valor, $tabla){
            $valor = $this->mysql->con->real_escape_string($valor);
			$valores = $this->unirUpdate($cabeceras, $nuevos, "=", ",");
            $sql = "UPDATE $tabla SET $valores WHERE $campo='$valor'";
			$resp = $this->mysql->ejecutar($sql);
			if($resp->affected_rows<1)
				return false;
			else
				return true;
		}
        public function editarPersonalizado($sql){
			$resp = $this->mysql->ejecutar($sql);
			if($resp->affected_rows<1)
				return false;
			else
				return true;
        }
		private function unir($separador, $datos){
			$cadena = "";
			for($i = 0; $i< count($datos); $i++){
                $datos[$i] = $this->mysql->con->real_escape_string($datos[$i]);
				if(is_numeric($datos[$i]))
					$cadena = $cadena.$datos[$i];
				else
					$cadena = $cadena."'".$datos[$i]."'";

				if($i != count($datos)-1)
					$cadena = $cadena.$separador;
			}
			return $cadena;
		}

		private function unirUpdate($cabeceras, $nuevos, $comparador, $separador){
			$cadena = "";
			for($i = 0; $i< count($nuevos); $i++){
                //$nuevos[$i] = $this->mysql->con->real_escape_string($nuevos[$i]);
				//if(is_numeric($nuevos[$i]))
					//$cadena = $cadena.$cabeceras[$i].$comparador.$nuevos[$i];
				//else
					$cadena = $cadena.$cabeceras[$i].$comparador."'".$nuevos[$i]."'";

				if($i != count($nuevos)-1)
					$cadena = $cadena.$separador;
			}
			return $cadena;
		}
        public function subirArchivo($archivo, $nombre, $destino){
            $nai = date("Ymdhis")."_".$nombre;
            $destino = $destino."/".$nai;
            $r = move_uploaded_file($archivo, $destino);
            if($r){
                return $destino;
            }else{
                return false;
            }
        }
		public function diastranscurridos($fecha_i,$fecha_f){
			$dias="";
			$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
			$dias 	= abs($dias); $dias = floor($dias);
			return $dias;

		}
		public function horastranscurridas($hora1,$hora2){
				$fecha1 = new DateTime('2018-01-01 '.$hora1.'');//fecha inicial
				$fecha2 = new DateTime('2018-01-01 '.$hora2.'');//fecha de cierre

				$intervalo = $fecha1->diff($fecha2);
                $hora=$intervalo->format('%H');
				$min=$intervalo->format('%i');
				return ' '.$hora.' '.(($hora>1)?'Horas':'Hora').' '.(($min>0)?' con '.$min.' Minutos':'').' ';

		}

        public function borrarArchivo($ruta){
            return unlink($ruta);
        }

		public function obtenerNroVisitas(){
			$archivo = "contador.json";
			$config = json_decode(file_get_contents($archivo), true);
            $contador = $config['nro_visitas'];
            return $contador;
		}

		public function insertarNroVisitas($nro){
			$archivo = "contador.json";
			$json = array("nro_visitas" => $nro);
            $n = file_put_contents($archivo, json_encode($json, true));
            if($n>0){
            	return true;
            }else{
            	return false;
            }
		}


		public function formatBytes($size){
		    $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
		    return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';
		}

		public function enviarCorreo($para, $mensaje, $asunto){
			
			$html = '';
			$html = '
			<table style="max-width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
			<tr>
				<td style="background-color:#ffa914;text-align: -webkit-center; padding: 10px;border-top-left-radius: 20px;border-top-right-radius: 20px;">
					<img style="padding: 0; display: block" src="https://i.postimg.cc/pVjQ5hds/20210722-161351.png" width="40%">
				</td>
			</tr>
			
			<tr style="border-left: #ffa914 solid 1px;border-right: #ffa914 solid 1px;" >
				<td style="padding: 0;background-color:white;" >
					<div style="text-align: center;color: #000000;">
						<h2 style="margin-top:20px;font-weight:bold" >Un cliente se encuentra interesado en tus servicios</h2>
		
						<div style="text-align: left;padding: 10px 30px ;" class="">
						'.$mensaje.'
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td style="background-color:#ffa914;text-align: -webkit-center; padding: 10px;border-bottom-left-radius: 20px;border-bottom-right-radius: 20px;">
				</td>
			</tr>
		
		</table>';
			

			$mail = new PHPMailer();
			// Activo condificacción utf-8
			$mail->CharSet = 'UTF-8';
			$mail->IsMail();
        	$mail->IsSMTP();
	        $mail->SMTPAuth = true;
	        $mail->Host = "mail.automotoresguatire.com.ve"; // host de donde se crea el email
	        $mail->Username = 'sistema@automotoresguatire.com.ve'; //email que utilizaras para enviar los correos
	        $mail->Password = '3T,(43hamYjN'; //contraseña del email
	        $mail->Port = 587;//puerto del email
	        $mail->SMTPSecure = 'SSL/TLS'; //puerto de seguridad
	        $mail->IsHTML(true);//si el cuerpo del email tendra html

	        $mail->From = "sistema@automotoresguatire.com.ve";//correo que puede recibir si alguien responde el email
	        $mail->FromName = "Automotoresguatire.com.ve";//nombre que aparece en la bandeja
	        $mail->Subject = $asunto; //asunto del email
	        $mail->AddAddress($para); //el correo a quien se le envia el email
	        $mail->Body = $html; //cuerpo del email

			if(!$mail->send()){
				return false;
        	}else{
            	return true;
            }
		}

    }
    $orm = new Orm($conx);
?>
