<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Solicitudes{
    private $tabla = "missolicitudes";
    public $data = [];
    public $orm = null;

    public function Solicitudes($tabla){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAll($id_usu){
      $sql = "SELECT *, S.id as ids, V.id as idv, M.id as idm  FROM ".$this->tabla." S, misvehiculos V, modelos_citas M WHERE S.id_vehiculo=V.id AND V.id_modelo=M.id AND S.id_usuario=$id_usu ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

    public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
