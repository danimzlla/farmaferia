<?php

if(isset($_POST)){
    include_once("../modelo/Orm.php");
    $nom = $_POST['fname'];
    $apellido = $_POST['lname'];
    $email = $_POST['email'];
    $asunto = $_POST['subject'];
    $mess = $_POST['message'];

    $mensaje = '';
    $mensaje.= '
            <h3>Nombre: '.$nom.'</h3>
            <h3>Apellido: '.$apellido.'</h3>
            <h3>Correo: '.$email.'</h3>
            <h3>Mensaje: '.$mess.'</h3>
    ';

    $para = "farmaferia@gmail.com";
    
    $resultado = $orm->enviarCorreo($para, $mensaje, $asunto);

    if(!$resultado){
        echo "<script>alert('No se ha podido enviar su correo, intentelo mas tarde.')</script>";
    } else {
        echo "<script>alert('Su correo fue enviado correctamente a farmaferia@gmail.com, le contactaremos a la brevedad posible.')</script>";
        echo "<script>window.location = 'https://farmaferia.com';</script>";
    }
}