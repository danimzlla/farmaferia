<div class="bg-productos" id="qbootstrap-blog">
	<div id="productos" class="container">
		<div class="row  animate-box">
			<div class="col-md-6 col-md-offset-3 text-center qbootstrap-heading">
				<h2>Nuestros productos</h2>
				<p class="text-dark" >Contamos con variedad de productos en el área de la salud.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row animate-box">
					<div class="main-carousel" data-flickity='{ "cellAlign": "left", "contain": true , "wrapAround": true , "freeScroll": true }'>
					
						<div class="cell">
							<div class="text-center active">
								<center class="" ><img src="static/img/medicinas/1.jpg" class="  img-producto borde " ></center>
								
								<div class="desc">
									<h3>Ibuprofeno</h3>
								</div>
							</div>
						</div>

						<div class="cell">
							<div class="text-center  active">
								<center class="" ><img src="static/img/medicinas/2.jpg" class="img-producto borde " ></center>
								
								<div class="desc">
									<h3>Femmex ultra</h3>
								</div>
							</div>
						</div>
						
						<div class="cell">
							<div class="text-center active">
								<center><img src="static/img/medicinas/3.jpg" class="img-producto borde " ></center>
								
								<div class="desc">
									<h3>Atamel</h3>
								</div>
							</div>
						</div>

						<div class="cell">
							<div class="text-center active">
								<center><img src="static/img/medicinas/5.jpeg" class=" img-producto borde"></center>
								
								<div class="desc">
								<h3>Línea para niños</h3>
								</div>
							</div>
						</div>

						<div class="cell">
							<div class="text-center active">
								<center><img src="static/img/medicinas/6.jpeg" class=" img-producto borde " ></center>
								
								<div class="desc">
								<h3>Crema cero</h3>
								</div>
							</div>
						</div>
						
						<div class="cell">
							<div class="text-center active">
								<center><img src="static/img/medicinas/7.jpeg" class=" img-producto borde " ></center>
								
								<div class="desc">
								<h3>Crema cero</h3>
								</div>
							</div>
						</div>
						
						<div class="cell">
							<div class="text-center active">
								<center><img src="static/img/medicinas/8.jpeg" class=" img-producto borde " ></center>
								
								<div class="desc">
								<h3>Toallas húmedas</h3>
								</div>
							</div>
						</div>
						
						<div class="cell">
							<div class="text-center active">
								<center><img src="static/img/medicinas/9.jpeg" class=" img-producto borde "></center>
								
								<div class="desc">
								<h3>Solución de rehidratación</h3>
								</div>
							</div>
						</div>
						
						<div class="cell">
							<div class="text-center active">
								<center><img src="static/img/medicinas/10.jpeg" class=" img-producto borde "></center>
								
								<div class="desc">
								<h3>Wampole</h3>
								</div>
							</div>
						</div>
						
					
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

