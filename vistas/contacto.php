<div id="qbootstrap-contact">
	<div id="contacto" class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 animate-box">
				<h3>Información de contacto</h3>
				<div class="row contact-info-wrap">
					<div class="col-md-6">
						<p><span><i class="fab fa-instagram"></i></span> <a href="https://instagram.com/farmaferia">farmaferia</a></p>
					</div>
					<div class="col-md-6">
						<p><span><i class="fas fa-mobile-alt"></i></span>0257-2511560 / <a href="https://api.whatsapp.com/send?phone=5804245039223&amp;text=Hola quiero saber información de: " target="_blank">0424-5039223</a></p>
					</div>
					<div class="col-md-6">
						<p><span><i class="fas fa-map-marker-alt"></i></span> Carrera 6 esquina calle 11, Guanare/Portuguesa/Venezuela</p>
					</div>
					<div class="col-md-6">
						<p><span><i class="fas fa-envelope"></i></span> <a href="mailto:info@farmaferia.com">info@farmaferia.com</a></p>
					</div>
				</div>
			</div>
			
			<div class="col-md-10 col-md-offset-1 animate-box">
				<div class="row">
					<div class="col-md-12">
						<h3>Escríbe tus inquietudes o solicita tu pedido aquí</h3>
					</div>
				</div>

				<form id="form-contacto" action="panel/controlador/contacto.php" method="post" role="form">
					<div class="row form-group">
						<div class="col-md-6">
							<label for="fname">Nombre</label>
							<input type="text" id="fname" name="fname" class="form-control" placeholder="Escribe tu nombre" required>
						</div>
						<div class="col-md-6">
							<label for="lname">Apellido</label>
							<input type="text" id="lname" name="lname" class="form-control" placeholder="Escribe tu apellido" required>
						</div>
					</div>

					<div class="row form-group">
						<div class="col-md-12">
							<label for="email">Correo electrónico</label>
							<input type="text" id="email" name="email" class="form-control" placeholder="Escribe tu correo electrónico" required>
						</div>
					</div>

					<div class="row form-group">
						<div class="col-md-12">
							<label for="subject">Asunto</label>
							<input type="text" id="subject" name="subject" class="form-control" placeholder="Escribe el asunto" required>
						</div>
					</div>

					<div class="row form-group">
						<div class="col-md-12">
							<label for="message">Mensaje</label>
							<textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Comentarios..." required></textarea>
						</div>
					</div>
					<div class="form-group text-center">
						<input id="submitBtn" type="submit" value="Enviar mensaje" class="btn btn-primary">
					</div>

				</form>		
			</div>
		</div>
	</div>
</div>

<a href="https://www.google.co.ve/maps/place/Farmacia+FarmaFeria/@9.0452881,-69.7494463,17z/data=!3m1!4b1!4m5!3m4!1s0x8e7cf5b69d9b847b:0x7f54283f222d5985!8m2!3d9.0452828!4d-69.7472576?hl=es&authuser=0" target="__blank" class="w-100 pointer">
	<img src="static/img/mapa.png" class="w-100"  alt="">
</a>

<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15760.810089408276!2d-69.7472576!3d9.0452828!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7f54283f222d5985!2sFarmacia%20FarmaFeria!5e0!3m2!1ses!2sve!4v1603376504362!5m2!1ses!2sve" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->