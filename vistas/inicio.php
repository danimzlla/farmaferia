<aside id="qbootstrap-hero">
	<div class="flexslider">
		<ul class="slides">
		<li class="bg-img-center" style="background-image: url(static/img/img_bg_1.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row flex flex-center">
					<div class="col-md-12 w-100 text-center slider-text">
						<div class="slider-text-inner text-center">
							<h1><strong> Su Farmacia de confianza </strong></h1>
						    <h2 class="doc-holder">Donde gana tu salud</h2>
							<p><a class="btn btn-primary btn-lg" href="#nosotros">Conoce más</a></p>
						</div>
					</div>
				</div>
			</div>
		</li>

		<li style="background-image: url(static/img/img_bg_3.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 w-100 text-center slider-text">
						<div class="slider-text-inner text-center">
							<h1><strong> Contamos con personal altamente capacitado</strong></h1>
						    <h2 class="doc-holder">Estamos dispuestos a atenderle siempre</h2>
							<p><a class="btn btn-primary btn-lg" href="#contacto"> Contáctanos</a></p>
						</div>
					</div>
				</div>
			</div>
		</li>

		<li style="background-image: url(static/img/img_bg_2.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row flex justify-center ">
					<div class="col-md-12 w-100 text-center slider-text">
						<div class="slider-text-inner">
							<h1><strong>Solicita tu pedido en línea </strong></h1>
							<h2 class="doc-holder">Contamos con Delivery</h2>
							<p><a class="btn btn-primary btn-lg btn-learn" href="https://wa.me/+58424-5039223" target="_blank">Solicitar Pedido </a></p>
						</div>
					</div>
				</div>
			</div>
		</li>		   	
		</ul>
	</div>
</aside>

<!-- <div class="">
	<img src="static\img\banner.png" class="w-100" alt="">
</div> -->

<div class="" id="qbootstrap-blog">
	<div id="productos" class="container ">
		<div class="row  animate-box">
			<div class="col-md-6 col-md-offset-3 text-center qbootstrap-heading">
				<h2> ¡Celebra nuestro 48 aniversario! </h2>
				<p class="text-dark" >Participa en todas las actividades que tenemos para ti.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row animate-box">
					<div class="main-carousel" data-flickity='{ "cellAlign": "left", "contain": true , "wrapAround": true , "freeScroll": true }'>
					
						<!--<div class="cell">
							<div class="text-center active">
								<center class="" ><img src="static/img/Noticias/noticia1.jpeg" class="  img-producto borde " ></center>
								
								
							</div>
						</div>-->

						<div class="cell">
							<div class="text-center active">
								<center class="" ><img src="static/img/Noticias/noticia2.jpeg" class="  img-producto borde " ></center>
								
								
							</div>
						</div>

						<div class="cell">
							<div class="text-center active">
								<center class="" ><img src="static/img/Noticias/noticia3.jpeg" class="  img-producto borde " ></center>
								
							</div>
						</div>

						<div class="cell">
							<div class="text-center active">
								<center class="" ><img src="static/img/Noticias/noticia4.jpeg" class="  img-producto borde " ></center>
							</div>
						</div>

						<div class="cell">
							<div class="text-center active">
								<center class="" ><img src="static/img/Noticias/noticia5.jpeg" class="  img-producto borde " ></center>
							</div>
						</div>

						<div class="cell">
							<div class="text-center active">
								<center class="" ><img src="static/img/Noticias/noticia6.jpeg" class="  img-producto borde " ></center>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="">
	<img src="static\img\banner.png" class="w-100" alt="">
</div>


<div id="qbootstrap-services">
	<div class="container">
		<div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center qbootstrap-heading">
				<h2>Garantía de servicio</h2>
				<p>Más de 40 años ofreciendo calidad y garantía de nuestro servicio</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 col-sm-6 col-xs-12 animate-box text-center">
				<div class="services  ">
					<span class="flex content-center mb-4 " ><i class="fas fa-thumbs-up font-xl icono-nosotros circle "></i></span>
					<h3><a href="#">Calidad de atención</a></h3>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-6 col-xs-12 animate-box text-center ">
				<div class="services  ">
					<span class="flex content-center mb-4 " ><i class="fas fa-user-nurse font-xl icono-nosotros circle "></i></span>
					<h3><a href="#">Contamos con profesionales</a></h3>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-6 col-xs-12 animate-box text-center ">
				<div class="services  ">
					<span class="flex content-center mb-4 " ><i class="fas fa-pills font-xl icono-nosotros circle "></i></span>
					<h3><a href="#">Variedad de productos</a></h3>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-6 col-xs-12 animate-box text-center ">
				<div class="services  ">
					<span class="flex content-center mb-4 " ><i class="fas fa-dollar-sign font-xl icono-nosotros circle "></i></span>
					<h3><a href="#">Mejores precios</a></h3>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-6 col-xs-12 animate-box text-center ">
				<div class="services  ">
					<span class="flex content-center mb-4 " ><i class="fas fa-head-side-mask font-xl icono-nosotros circle "></i></span>
					<h3><a href="#">Cuidamos las normas de bioseguridad</a></h3>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-6 col-xs-12 animate-box text-center ">
				<div class="services">
					<span class="flex content-center mb-4 " ><i class="fas fa-heart font-xl icono-nosotros circle "></i></span>
					<h3><a href="#">Comprometidos con tu salud</a></h3>
				</div>
			</div>
		</div>
	</div>
</div>

        <div id="servicios" class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center qbootstrap-heading ">
				<h2> Nuestros servicios</h2>
			    <p>¡Te ofrecemos todo esto y más! </p>
			</div>
		</div>


<div id="qbootstrap-services" class="animate-box pt-0 container row-flex">

    <div style=" background-image: url('static/img/servicios/servicio1.jpg');" class=" capa col-xs-12 col-lg-3 p-0 flex item-center ">
		<div class="texto row-flex">
			<h5 class="text-white col-lg-12 text-center w-100" >Autofarmacia</h5> 
			<p class="col-lg-12" >8:00 am a 8:00 pm Lunes a sábado </p>
			<p class="col-lg-12" >Domingos 8:00 am a 6:00 pm</p>

		</div>
	</div>

	<div style=" background-image: url('static/img/servicios/servicio2.jpg');" class=" capa col-xs-12 col-lg-3 p-0 flex item-center ">
		<div class="texto row-flex">
			<h5 class="text-white col-lg-12 text-center w-100" >Toma de tensión</h5> 
			<p class="col-lg-12" >Totalmente gratuita</p>
		</div>
	</div>

	<div style=" background-image: url('static/img/servicios/servicio3.jpg');" class=" capa col-xs-12 col-lg-3 p-0 flex item-center ">
		<div class="texto row-flex">
			<h5 class="text-white col-lg-12 text-center w-100" >Atención Personalizada</h5> 
			<p class="col-lg-12" >Nos adaptamos a tu necesidades</p>
		</div>
	</div>

	<div style=" background-image: url('static/img/servicios/servicio4.jpg');" class=" capa col-xs-12 col-lg-3 p-0 flex item-center ">
		<div class="texto row-flex">
			<h5 class="text-white col-lg-12 text-center w-100" >Delivery</h5> 
			<p class="col-lg-12" >Disponible en todo el horario de atención</p>
		</div>
	</div>

</div>


<!-- <div id="cuestionario" class="mb-5 pb-5 "></div> -->

<!-- Idea 1 -->
<!-- <div id="todo" class=" my-5 hide ">

        <div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center ">
				<h2> ¡Pues nosotros te ofrecemos todo eso y más!</h2>
			
			</div>
		</div>

<div class=" row-flex content-around " >

	<div class="col-12 col-md-6 my-5 animate-box">

	<div class=" col-md-12 px-3 card_type  ">

		<div class="col-md-6 p-3 ">
			<img src="static\img\blog-1.jpg" class="w-100 soft-border" alt="">
		</div>

		<div class="col-md-6 row p-3 ">

		<div class="col-md-12 text-center ">
			<h3>Autofarmacia</h3>
		</div>

		<div class="col-md-12">
			<p class="mb-0 font-sm text-justify " >Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium voluptate corporis similique nisi magnam, veniam omnis amet, sint quidem pariatur fugiat sunt ipsum doloribus illo?</p>
		</div>

		</div>

	</div>

	</div>

    <div class="col-12 col-md-6 my-5 animate-box">

	   <div class=" col-md-12 px-3 card_type  ">

			<div class="col-md-6 p-3 ">
				<img src="static\img\blog-1.jpg" class="w-100 soft-border" alt="">
			</div>

			<div class="col-md-6 row p-3 ">

			<div class="col-md-12 text-center ">
				<h3>Toma de tensión</h3>
			</div>

			<div class="col-md-12">
				<p class="mb-0 font-sm text-justify " >Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium voluptate corporis similique nisi magnam, veniam omnis amet, sint quidem pariatur fugiat sunt ipsum doloribus illo?</p>
			</div>

			</div>

	   </div>

	</div>

	<div class="col-12 col-md-6 my-5 animate-box">

	   <div class=" col-md-12 px-3 card_type  ">

			<div class="col-md-6 p-3 ">
				<img src="static\img\blog-1.jpg" class="w-100 soft-border" alt="">
			</div>

			<div class="col-md-6 row p-3 ">

			<div class="col-md-12 text-center ">
				<h3>Atención Personalizada</h3>
			</div>

			<div class="col-md-12">
				<p class="mb-0 font-sm text-justify " >Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium voluptate corporis similique nisi magnam, veniam omnis amet, sint quidem pariatur fugiat sunt ipsum doloribus illo?</p>
			</div>

			</div>

	   </div>

	</div>

	<div class="col-12 col-md-6 my-5 animate-box">

	   <div class=" col-md-12 px-3 card_type  ">

			<div class="col-md-6 p-3 ">
				<img src="static\img\blog-1.jpg" class="w-100 soft-border" alt="">
			</div>

			<div class="col-md-6 row p-3 ">

			<div class="col-md-12 text-center ">
				<h3>Delivery</h3>
			</div>

			<div class="col-md-12">
				<p class="mb-0 font-sm text-justify " >Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium voluptate corporis similique nisi magnam, veniam omnis amet, sint quidem pariatur fugiat sunt ipsum doloribus illo?</p>
			</div>

			</div>

	   </div>

	</div>

</div>
</div> -->

<!-- Cuestionario 1 -->
<!-- <div id="pregunta1" class=" my-5 ">

        <div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center ">
				<h2> ¿Estas intereaso en?</h2>
				
			</div>
		</div>

		

<div class=" row-flex content-around animate-box" >

	<div class="col-12 col-md-6 my-5 ">

	   <div class=" col-md-12 px-3 card_type  ">

			<div class="col-md-6 p-3 ">
				<img src="static\img\blog-1.jpg" class="w-100 soft-border" alt="">
			</div>

			<div class="col-md-6 row p-3 ">

			<div class="col-md-12 text-center ">
				<h3>Autofarmacia</h3>
			</div>

			<div class="col-md-12">
				<p class="mb-0 font-sm text-justify " >Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium voluptate corporis similique nisi magnam, veniam omnis amet, sint quidem pariatur fugiat sunt ipsum doloribus illo?</p>
			</div>

			</div>

	   </div>

	</div>

</div>

        <div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center ">
			<p><a class="btn btn-primary btn-lg" onclick="pregunta1()" href="#D">Claro que si</a></p>
			</div>
		</div>

</div> -->
<!-- cuestionario 1 -->

<!-- Cuestionario 2 -->
<!-- <div id="pregunta2" class=" my-5 hide">

        <div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center ">
				<h2> ¿Y que tal en?</h2>
				
			</div>
		</div>

		

<div class=" row-flex content-around animate-box" >

	<div class="col-12 col-md-6 my-5 ">

	   <div class=" col-md-12 px-3 card_type  ">

			<div class="col-md-6 p-3 ">
				<img src="static\img\blog-1.jpg" class="w-100 soft-border" alt="">
			</div>

			<div class="col-md-6 row p-3 ">

			<div class="col-md-12 text-center ">
				<h3>Toma de tensión</h3>
			</div>

			<div class="col-md-12">
				<p class="mb-0 font-sm text-justify " >Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium voluptate corporis similique nisi magnam, veniam omnis amet, sint quidem pariatur fugiat sunt ipsum doloribus illo?</p>
			</div>

			</div>

	   </div>

	</div>

</div>

        <div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center ">
			<p><a class="btn btn-primary btn-lg" onclick="pregunta2()" href="#D">Por supuesto</a></p>
			</div>
		</div>

</div> -->
<!-- cuestionario 2 -->

<!-- Cuestionario 3 -->
<!-- <div id="pregunta3" class=" my-5 hide">

        <div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center ">
				<h2> ¿Y sobre?</h2>
				
			</div>
		</div>

		

<div class=" row-flex content-around animate-box" >

	<div class="col-12 col-md-6 my-5 ">

	   <div class=" col-md-12 px-3 card_type  ">

			<div class="col-md-6 p-3 ">
				<img src="static\img\blog-1.jpg" class="w-100 soft-border" alt="">
			</div>

			<div class="col-md-6 row p-3 ">

			<div class="col-md-12 text-center ">
				<h3>Atención Personalizada</h3>
			</div>

			<div class="col-md-12">
				<p class="mb-0 font-sm text-justify " >Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium voluptate corporis similique nisi magnam, veniam omnis amet, sint quidem pariatur fugiat sunt ipsum doloribus illo?</p>
			</div>

			</div>

	   </div>

	</div>

</div>

        <div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center ">
			<p><a class="btn btn-primary btn-lg" onclick="pregunta3()" href="#D">Si me interesa</a></p>
			</div>
		</div>

</div> -->
<!-- cuestionario 3 -->

<!-- Cuestionario 4 -->
<!-- <div id="pregunta4" class=" my-5 hide ">

        <div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center ">
				<h2> Por ultimo ¿te interesa el servicio de Delivery? </h2>
				
			</div>
		</div>

		

<div class=" row-flex content-around animate-box" >

	<div class="col-12 col-md-6 my-5 ">

	   <div class=" col-md-12 px-3 card_type  ">

			<div class="col-md-6 p-3 ">
				<img src="static\img\blog-1.jpg" class="w-100 soft-border" alt="">
			</div>

			<div class="col-md-6 row p-3 ">

			<div class="col-md-12 text-center ">
				<h3>Delivery</h3>
			</div>

			<div class="col-md-12">
				<p class="mb-0 font-sm text-justify " >Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium voluptate corporis similique nisi magnam, veniam omnis amet, sint quidem pariatur fugiat sunt ipsum doloribus illo?</p>
			</div>

			</div>

	   </div>

	</div>

</div>

        <div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center ">
			<p><a class="btn btn-primary btn-lg" onclick="pregunta4()" href="#cuestionario">Si me ineteresa</a></p>
			</div>
		</div>

</div> -->
<!-- cuestionario 4 -->


<!-- Productos -->

<?php include("producto.php"); ?>

<!-- fin productos -->

<!-- carrera
<div id="qbootstrap-blog">
	<div  class="row intro animate-box">
		<div class="col-md-6 col-sm-12 p-0 half bg-img" style="background-image: url(static/img/carrera/posterweb.jpg);" >
		</div>
		
		<div class="col-md-6 col-sm-12 bg-primary p-5">
			<div class="row">
				<div class="col-md-12 p-0 flex item-center animate-box">
					<div class="ml-3 mr-3">
						<p class="fas fa-running icono circle"></p>
					</div>
					
					<div class="">
						<h2 class="text-white ml-2">Gran carrera por la salud</h2>
						<p class="" >Acompañanos a celebrar nuestro 45 aniversario "Carrera/Caminata de la salud farmaferia" vive la experiencia 2020 donde gana tu salud.</p> 
					</div>
				</div>

				<div class="col-md-12 p-0 mt-3 flex item-center animate-box">
					<div class="ml-3 mr-3">
						<p class="far fa-address-card icono circle"></p>
					</div>

					<div class="">
						<h2 class="text-white ml-2 " >Inscripciones abiertas</h2>
						<ul>
							<li>8$ Individual</li>
							<li>5$ Por grupos (5 integrantes)</li>
						</ul>
					</div>
				</div>

				<div class="col-md-12 p-0 mt-3 flex item-center animate-box">
					<div class="ml-3 mr-3">
						<p class="fas fa-gifts icono circle"></p>
					</div>

					<div class="">
						<h2 class="text-white ml-2">Incluye</h2>
						<ul>
							<li>Número de participación</li>
							<li>Medalla</li>
							<li>Hidratación</li>
							<li>Medidas de salud y bioseguridad</li>
						</ul>
					</div>
				</div>

				<div class="col-md-12 p-0 mt-3 flex item-center animate-box">
					<div class="ml-3 mr-3">
						<p class="fas fa-comment-dollar icono circle"></p>
					</div>

					<div class="">
						<h2 class="text-white ml-2 " >Formas de pago</h2>
						<ul>
							<li>Pago móvil</li>
							<li>Efectivo</li>
							<li>Transferencia bancaria</li>
							<li>Zelle</li>
						</ul>
					</div>
				</div>

				<div class="col-md-12 p-0 mt-3 flex item-center animate-box">
					<div class="text-white text-center">
						<h2 class="text-white" >Lo recaudado sera donado a obras sociales</h2>
						<h2 class="text-white" ><span class="fab fa-whatsapp"></span> 0424-5039293</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 fin carrera -->

<!-- <div id="qbootstrap-choose">
	<div class="container-fluid">
		<div class="row">
			<div class="choose">
				<div class="half img-bg" style="background-image: url(static/img/img_bg_6.jpg);"></div>
				<div class="half features-wrap">
					<div class="qbootstrap-heading animate-box">
						<h2>What Makes Us the Best?</h2>
						<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
					</div>
					<div class="features animate-box">
						<span class="icon text-center"><i class="icon-group-outline"></i></span>
						<div class="desc">
							<h3>Well Experienced Doctors</h3>
							<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
						</div>
					</div>
					<div class="features animate-box">
						<span class="icon text-center"><i class="icon-flow-merge"></i></span>
						<div class="desc">
							<h3>Free Medical Consultation</h3>
							<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
						</div>
					</div>
					<div class="features animate-box">
						<span class="icon text-center"><i class="icon-document-text"></i></span>
						<div class="desc">
							<h3>Online Enrollment</h3>
							<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
						</div>
					</div>
					<div class="features animate-box">
						<span class="icon text-center"><i class="icon-gift"></i></span>
						<div class="desc">
							<h3>Modern Facilities</h3>
							<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->





<!-- Nosotros -->

<div id="qbootstrap-blog">
	<div id="nosotros" class="container">
		<div class="row intro animate-box pb-7r ">
			<div class="col-md-12 col-sm-12 w-100 text-center pb-5">
			  <h2>Sobre nosotros</h2>
			  <p>Estos 46 años hemos estado a tu lado </p>
			</div>

			<div  class="col-md-6 col-sm-12 p-0 half img-bg " >
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item active " role="presentation">
						<a class="nav-link " id="nosotros-tab" data-toggle="tab" href="#nosotros" role="tab" aria-controls="Nosotros" aria-selected="true">Nosotros</a>
					</li>

					<li class="nav-item  " role="presentation">
						<a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Historia</a>
					</li>

					<li class="nav-item" role="presentation">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Misión</a>
					</li>

					<li class="nav-item" role="presentation">
						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Visión</a>
					</li>
				</ul>
            

				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade in active  text-left ml-4 " id="nosotros" role="tabpanel" aria-labelledby="nosotros-tab">
						<p></p>
						<p>Farmacia con tradición familiar pasada de generación en generación, conservando la esencia con la que iniciamos hace 46 Años  brindando atención y Excelente servicio. </p>
						<p>Nos mantenemos a la vanguardia, dedicados a la mejora y novedad en la presentación de nuestros productos, adaptándonos a los cambios.</p>
					</div>
					<div class="tab-pane fade text-left ml-4 " id="home" role="tabpanel" aria-labelledby="home-tab">
						<p></p>
						<p>El Dr. José Gómez Álvarez, funda su primera farmacia, en el año de 1953 FARMACIA LA COROMOTO. </p>
						<p> Luego el 6 de Noviembre de 1974 Funda su segunda Farmacia. FARMACIA LOS COSPES. C.A </p>
						<p>Fue un crecimiento vertiginoso  por su reconocida trayectoria el que experimentó durante todos eso años,  dándose paso en armonía con aquella Venezuela  que se levantaba y desarrollaba en esos tiempos.</p>
						<p>Ahora el grupo ha transformado sus farmacias bajo el nombre corporativo de Grupo: FARMAFERIA.</p>
					</div>

					<div class="tab-pane fade text-left ml-4 " id="profile" role="tabpanel" aria-labelledby="profile-tab">
						<p></p>
						<p>Ser la Mejor Empresa prestadora de Servicios de Salud, que brinda al cliente el más completo stock de medicamentos de  productos y servicios, para el bienestar de la población y el mejoramiento de su calidad de Vida.</p>
					</div>

					<div class="tab-pane fade text-left ml-4 " id="contact" role="tabpanel" aria-labelledby="contact-tab">
						<p></p>
						<p>Convertirnos en la Organización más Grande de  reconocida trayectoria en la región por ser la Empresa que presta un servicio de salud de Optima Calidad a la Población, brindándole el mejor trato y garantizando el medicamento a tiempo con la finalidad de  contribuir con el mejoramiento de su salud.</p>
					</div>
				</div>
			</div>
			
			<div  class="col-md-6 col-sm-12">

				<div id="carousel-1" class="carousel slide" data-ride="carousel">
					
					<ol class="carousel-indicators">
						<li data-target="#carousel-1" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-1" data-slide-to="1"></li>
						<li data-target="#carousel-1" data-slide-to="2"></li>
						<li data-target="#carousel-1" data-slide-to="3"></li>
					</ol>
					
					<div class="carousel-inner" role="listbox">
						<div class="item img-historia ">
							<img src="static/img/Historia/Historia3.jpg" class="img-historia"  alt="">
						</div>

						<div class="item img-historia ">
							<img src="static/img/Historia/Historia4.jpg" class="img-historia"  alt="">
						</div>

						<div class="item active img-historia ">
							<img src="static/img/Historia/Historia2.jpeg" class="img-historia" alt="">
						</div>

						<div class="item img-historia ">
							<img src="static/img/Historia/Historia1.jpeg" class="img-historia"  alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<!-- fin nosotros -->
<div id="qbootstrap-register" style="background-image: url(static/img/img_bg_3.jpg);" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 animate-box">
				<div class="date-counter text-center">
					<h2>Más de <strong class="color">2000</strong> medicinas</h2>
					<p><a href="https://api.whatsapp.com/send?phone=5804245039223&amp;text=Hola quiero saber información de: " target="_blank" class="btn btn-primary btn-lg"><i class="fab fa-whatsapp"></i> Consulta ahora </a></p>
				</div>
			</div>
		</div>
	</div>
</div>

<!--
<div id="qbootstrap-blog">
	<div class="container">
		<div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center qbootstrap-heading">
				<h2>Noticias</h2>
				<p>Conoce nuestros últimos eventos y actividades.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row animate-box">
					<div class="owl-carousel owl-carousel-fullwidth">
						<div class="item">
							<div class="blog-slide active">
								<a href="blog.html" class="blog-box" style="background-image: url(static/img/blog-1.jpg);">
									<span class="date">Aug 20, 2020</span>
								</a>
								<div class="desc">
									<h3><a href="blog.html">2020 Triathlon Event</a></h3>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="blog-slide active">
								<a href="blog.html" class="blog-box" style="background-image: url(static/img/blog-2.jpg);">
									<span class="date">Aug 20, 2020</span>
								</a>
								<div class="desc">
									<h3><a href="blog.html">Cycling gives more health beneficial</a></h3>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="blog-slide active">
								<a href="blog.html" class="blog-box" style="background-image: url(static/img/blog-3.jpg);">
									<span class="date">Aug 20, 2020</span>
								</a>
								<div class="desc">
									<h3><a href="blog.html">USA, International Triathlon Event</a></h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="qbootstrap-counter" class="qbootstrap-counters" style="background-image: url(static/img/img_bg_2.jpg);" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="row">
					<div class="col-md-3 col-sm-6 text-center animate-box">
						<span class="icon"><i class="icon-group-outline"></i></span>
						<span class="qbootstrap-counter js-counter" data-from="0" data-to="3297" data-speed="5000" data-refresh-interval="50"></span>
						<span class="qbootstrap-counter-label">Satisfied Customer</span>
					</div>
					<div class="col-md-3 col-sm-6 text-center animate-box">
						<span class="icon"><i class="icon-home-outline"></i></span>
						<span class="qbootstrap-counter js-counter" data-from="0" data-to="378" data-speed="5000" data-refresh-interval="50"></span>
						<span class="qbootstrap-counter-label">Hospitals</span>
					</div>
					<div class="col-md-3 col-sm-6 text-center animate-box">
						<span class="icon"><i class="icon-user-add-outline"></i></span>
						<span class="qbootstrap-counter js-counter" data-from="0" data-to="400" data-speed="5000" data-refresh-interval="50"></span>
						<span class="qbootstrap-counter-label">Qualified Doctor</span>
					</div>
					<div class="col-md-3 col-sm-6 text-center animate-box">
						<span class="icon"><i class="icon-point-of-interest-outline"></i></span>
						<span class="qbootstrap-counter js-counter" data-from="0" data-to="30" data-speed="5000" data-refresh-interval="50"></span>
						<span class="qbootstrap-counter-label">Departments</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->

<?php include("contacto.php"); ?>