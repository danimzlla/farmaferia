<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	session_start();
	include_once('ruta.php');
?>

<!DOCTYPE html>
<html class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Farmaferia</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Farmaferia, donde gana tu salud... Personal altamente capacitado y dedicado a las necesidades de nuestros clientes." />
		<meta name="keywords" content="Farmacia, salud, medicinas, online, autofarmacia" />
		<meta name="author" content="Farmaferia" />

		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />

		<link rel="icon" href="static/img/favicon.ico">
		
		<link rel="stylesheet" href="static/css/animate.css">
		<link rel="stylesheet" href="static/css/bootstrap.css">
		<link rel="stylesheet" href="static/css/font-awesome.min.css">
		<link rel="stylesheet" href="static/css/magnific-popup.css">
		<link rel="stylesheet" href="static/css/owl.carousel.min.css">
		<link rel="stylesheet" href="static/css/owl.theme.default.min.css">
		<link rel="stylesheet" href="static/css/flexslider.css">
		<link rel="stylesheet" href="static/css/style.css">
		<link rel="stylesheet" href="static/css/flickity.css">
		<link rel="stylesheet" href="static/css/Estilos.css">
		<script src="static/js/modernizr-2.6.2.min.js"></script>
	</head>
	<body>
		
	<!--<div class="qbootstrap-loader"></div>-->
	
	<div  id="page">
		<nav id="navbar-responsive" class="qbootstrap-nav" role="navigation">
			<div class="row">
				<div  class="col-xs-12">
					<div class="top">
						<div class="row">
							<div id="Inicio" class="col-md-4 col-md-push-4 text-center">
								<div id="qbootstrap-logo"><a href="#"><img src="static/img/logo.png" class="img-logo" ></a></div>
							</div>
						</div>
					</div>
				</div>

				<div id="navbar" class="col-xs-12 text-center p-0">
					<div class="menu-1">
						<ul>
							<li class="active"><a href="#Inicio">Inicio</a></li>
							<li><a href="#servicios">Servicios</a></li>
							<li><a href="#productos">Productos</a></li>
							<li><a href="#nosotros">Nosotros</a></li>
							<li><a href="#contacto">Contacto</a></li>
							<li class="btn-cta"><a href="https://api.whatsapp.com/send?phone=5804245039223&amp;text=Hola quiero saber información de: " target="_blank"><span>Whatsapp <i class="fab fa-whatsapp"></i></span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		
		<?php include($ruta); ?>

		<div class="bg-feed" style="margin-top: -1rem;">
			<div class="row row-pb-md">
				<div class="col-md-12">
					<script src="https://apps.elfsight.com/p/platform.js" defer></script>
					<div class="elfsight-app-63882c61-6a56-4392-852e-68d1e49992ed"></div>
				</div>
			</div>
		</div>

		<footer class="bg-dark bg-black " role="contentinfo ">
			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p class="mb-0 py-3" > 
	                    <small class="block">&copy;2022 Farmaferia</small> 
	                    <small class="block">Desarrollado por <a href="https://pladecompany.com/" target="_blank">Plade Company C.A</a></small>
	                </p>
				</div>
			</div>
		</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="fas fa-arrow-up"></i></a>
	</div>
	<!-- <script src="static\js\cuestionario.js" ></script> -->
	<script src="static/js/jquery.min.js"></script>
	<script src="static/js/jquery.easing.1.3.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
	<script src="static/js/jquery.waypoints.min.js"></script>
	<script src="static/js/jquery.stellar.min.js"></script>
	<script src="static/js/owl.carousel.min.js"></script>
	<script src="static/js/jquery.flexslider-min.js"></script>
	<script src="static/js/jquery.countTo.js"></script>
	<script src="static/js/jquery.magnific-popup.min.js"></script>
	<script src="static/js/magnific-popup-options.js"></script>
	<script src="static/js/sticky-kit.min.js"></script>
	<script src="static/js/main.js"></script>
	<script src="static/js/flickity.js"></script>
	<script>
	window.onscroll = function (){
		let altura = window.pageYOffset;
		if( altura >= 147){
         document.getElementById("navbar").classList.add ('fixed-nav')
		}
		else if(altura <= 146){
		 document.getElementById("navbar").classList.remove ('fixed-nav')
		}
		if(altura >= 0){
         document.getElementById("navbar-responsive").classList.add ('fixed-nav-responsive')
		}
	}
	</script>

     <script>
	  
	</script>
	<!-- <script>
	window.onscroll = function (){
		let altura = window.pageYOffset;
		if( altura >= 0){
         document.getElementById("navbar-responsive").classList.add ('fixed-nav-responsive')
		}
	}
	</script> -->

	<script>
		$( document ).ready(function(){
			$('.carousel').carousel({
				interval: 2000
			});
		});
	</script>

	</body>
</html>

