-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 30-11-2020 a las 15:32:15
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `farmaferia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `usuario` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `acceso` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `usuario`, `password`, `correo`, `acceso`) VALUES
(1, 'Administrador', '827ccb0eea8a706c4c34a16891f84e7b', 'admin', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agentes`
--

CREATE TABLE `agentes` (
  `id` int(11) NOT NULL,
  `cod_age` varchar(20) DEFAULT NULL,
  `nom_age` varchar(20) DEFAULT NULL,
  `ape_age` varchar(25) DEFAULT NULL,
  `tlf_age` varchar(15) DEFAULT NULL,
  `img_age` text DEFAULT NULL,
  `est_age` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `agentes`
--

INSERT INTO `agentes` (`id`, `cod_age`, `nom_age`, `ape_age`, `tlf_age`, `img_age`, `est_age`) VALUES
(1, '00001', 'Leonardo', 'Diaz', '04120545738', 'https://exiauto.com/static/img/user.png', 1),
(2, '00002', 'Ivan', 'Toro', '04120545738', 'https://exiauto.com/static/img/user.png', 1),
(3, '00003', 'Danny', 'Ascanio', '04120545738', 'https://exiauto.com/static/img/user.png', 1),
(4, '00004', 'Marlon', 'Ramirez', '', 'https://exiauto.com/static/img/user.png', 1),
(5, '00005', 'Alex', 'Garcia', '', 'https://exiauto.com/static/img/user.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_repuestos`
--

CREATE TABLE `categorias_repuestos` (
  `id` int(11) NOT NULL,
  `nom_cat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_vehiculo` int(11) DEFAULT NULL,
  `id_agente` int(11) DEFAULT NULL,
  `motivo` text DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `estatus` int(11) DEFAULT NULL,
  `fec_env` datetime DEFAULT NULL,
  `fac_res` datetime DEFAULT NULL,
  `tipo_cita` varchar(25) DEFAULT NULL,
  `id_kilometros` int(11) DEFAULT NULL,
  `id_falla` int(11) DEFAULT NULL,
  `observacion` text DEFAULT NULL,
  `reprogramado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `citas`
--

INSERT INTO `citas` (`id`, `id_usuario`, `id_vehiculo`, `id_agente`, `motivo`, `fecha`, `estatus`, `fec_env`, `fac_res`, `tipo_cita`, `id_kilometros`, `id_falla`, `observacion`, `reprogramado`) VALUES
(48, 1, 4, 2, 'Hola como estas vnzla', '2019-12-26 09:30:00', -1, '2019-12-21 12:47:00', NULL, 'Mantenimiento períodico', 4, 201, NULL, 0),
(49, 1, 3, 1, 'Probando citas', '2019-12-25 13:30:00', -1, '2019-12-21 12:55:27', NULL, 'Garantía TDV (Vehículo nu', 3, 201, 'Todo bien.', 0),
(50, 1, 4, 1, 'testing this module.', '2019-12-26 13:30:00', -2, '2019-12-21 13:13:26', NULL, 'Garantía de servicio (Ret', 1, 70, 'no tenemos disponibilidad para ese dia.', 0),
(51, 1, 4, 5, 'ok o ko ok ok ', '2019-12-25 08:45:00', -1, '2019-12-21 13:49:02', NULL, 'Mantenimiento períodico', 4, 201, NULL, 0),
(52, 15, 12, 1, 'Prueba', '2019-12-23 07:30:00', -1, '2019-12-21 15:32:33', NULL, 'Mantenimiento períodico', 200, 201, '', 0),
(53, 6, 13, 2, 'hola', '2020-01-23 08:00:00', -1, '2019-12-22 12:05:00', NULL, 'Mantenimiento períodico', 85, 201, NULL, 0),
(54, 15, 11, 1, 'Prueba de Sistemas...', '2020-01-21 08:15:00', -2, '2020-01-16 14:25:53', NULL, 'Mantenimiento períodico', 230, 201, '', 0),
(55, 15, 11, 1, 'prueba de sistemas', '2020-01-20 07:30:00', 2, '2020-01-16 14:45:19', NULL, 'Mantenimiento períodico', 230, 201, '', 0),
(56, 22, 14, 3, 'En estos momentos estoy de viaje y regreso el 25/01 de manera que les pido me asignen cita para la última semana de enero de ser posible. El carro no presenta falla mecánica de manera que lo voy a estar llevando el día que me asignen la cita. Hay que hacerle una revisión de los frenos. Les pido que cualquier pregunta me la hagan llegar a mi email ya que estoy fuera del país. Saludos', '2020-01-27 08:00:00', 2, '2020-01-20 12:22:14', NULL, 'Mantenimiento períodico', 80, 201, '', 0),
(57, 25, 15, 5, 'Si apago el carro cuando esta caliente y lo vuelvo a encender, vibra y se apaga', '2020-01-24 08:30:00', 2, '2020-01-21 09:51:00', NULL, 'Revisión general', 1, 90, 'Su cita fue programada para el día 28 de enero 8:30 am', 0),
(58, 15, 11, 2, 'Prueba de Sistemas', '2020-01-22 08:15:00', -2, '2020-01-21 13:10:12', NULL, 'Mantenimiento períodico', 230, 201, '', 0),
(59, 29, 16, 4, 'El vevhiculo es una toyota Meru', '2020-01-29 08:15:00', 2, '2020-01-24 11:22:06', NULL, 'Revisión general', 1, 140, '', 0),
(60, 6, 13, 4, 'SE SIENTE RARA', '2020-02-07 07:30:00', -2, '2020-02-05 10:30:34', NULL, 'Garantía de servicio (Ret', 1, 20, '', 0),
(61, 9, 8, 1, 'Quiero una revisión general', '2020-02-11 10:15:00', 1, '2020-02-07 17:01:25', NULL, 'Revisión general', 1, 60, '', 0),
(62, 40, 19, 3, 'Buenos dias, el carro presenta una falla de entonación del motor. Adicional los seguros del carro, no estan funcionando con la alarma, sino que hay que abrir o cerrar el carro de forma manuela', '2020-02-28 09:00:00', 1, '2020-02-26 11:35:37', NULL, 'Revisión general', 1, 1, '', 1),
(63, 43, 21, 1, 'El carro está un poco por encima de los 10,000 Km', '2020-03-02 08:00:00', 2, '2020-02-29 12:05:21', NULL, 'Mantenimiento períodico', 3, 201, '', 0),
(64, 13, 10, 3, 'mantenimiento y ajustes generales', '2020-03-09 07:30:00', 1, '2020-03-02 09:38:17', NULL, 'Mantenimiento períodico', 5, 201, 'No tenemos disponibilidad a las 7:30 am sino a las 8:30 am', 0),
(65, 45, 22, 2, 'Revision', '2020-03-16 07:30:00', 1, '2020-03-10 10:01:15', NULL, 'Mantenimiento períodico', 180, 201, 'TENEMOS DISPONIBILIDAD A LAS 8:30AM', 0),
(66, 1, 12, 2, 'Cita de prueba, soy el programador.', '2020-03-12 15:30:00', -1, '2020-03-11 09:27:43', NULL, 'Mantenimiento períodico', 5, 201, NULL, 0),
(67, 46, 23, 1, 'Diagnostico completo del vehiculo con informe. revisar ruido en la parte delantera. diagnostico tripoides. limpieza de inyectores. revision de frenos. revision y diagnostico refrigerante. aire acondicionado. cambio de filtros.', '2020-03-16 07:30:00', 1, '2020-03-11 10:26:08', NULL, 'Revisión general', 1, 1, '', 0),
(68, 48, 24, 5, 'Entiendo que también hay un llamado para cambiar la bolsa de aire', '2020-03-16 08:00:00', 1, '2020-03-13 12:13:06', NULL, 'Revisión general', 1, 110, '', 0),
(69, 1, 12, 3, 'test', '2020-03-19 09:45:00', -1, '2020-03-15 02:27:15', NULL, 'Mantenimiento períodico', 4, 201, NULL, 0),
(70, 1, 12, 1, 'testing', '2020-03-18 08:15:00', -1, '2020-03-15 02:28:49', NULL, 'Mantenimiento períodico', 5, 201, NULL, 0),
(71, 1, 12, 1, 'testing', '2020-03-18 09:30:00', -1, '2020-03-15 02:32:07', NULL, 'Mantenimiento períodico', 5, 201, NULL, 0),
(72, 1, 12, 4, 'asdadasd', '2020-03-19 08:15:00', -1, '2020-03-15 02:34:06', NULL, 'Revisión general', 1, 30, NULL, 0),
(73, 1, 12, 1, 'testing cita', '2020-03-18 08:00:00', -2, '2020-03-15 02:39:30', NULL, 'Mantenimiento períodico', 5, 201, '', 0),
(74, 49, 26, 1, 'Mantenimiento', '2020-05-11 07:30:00', 2, '2020-05-05 12:28:17', NULL, 'Mantenimiento períodico', 115, 201, '', 0),
(75, 53, 29, 3, 'Quisiera conocer el costo y que incluye, adicional una revisión general y requiero una cita con latoneria', '2020-08-10 07:30:00', 2, '2020-08-04 09:19:49', NULL, 'Mantenimiento períodico', 130, 201, '', 0),
(76, 56, 30, 2, 'nose', '2020-08-20 09:15:00', -1, '2020-08-19 22:47:25', NULL, 'Mantenimiento períodico', 190, 201, '', 0),
(77, 57, 31, 4, 'ademas de cambio de aceite ', '2020-08-31 09:00:00', 1, '2020-08-26 14:51:29', NULL, 'Revisión general', 1, 100, '', 0),
(78, 58, 34, 4, 'favor hacer revision completa de los inyectores', '2020-09-03 08:00:00', 1, '2020-08-27 16:24:42', NULL, 'Mantenimiento períodico', 60, 201, '', 0),
(79, 59, 35, 3, 'Cita para lo del llamado de Toyota y para que por favor revisen qué ocurre con mi tanque de gas ya que no enciende y no puedo utilizar el gas, solo gasolina.', '2020-09-15 07:30:00', 1, '2020-09-07 22:00:39', NULL, 'Revisión general', 1, 201, 'Buenas tardes \n(V-7418756) Maria Lissett Rodriguez de Andrade. En respuesta a su solicitud las campañas de Toyota corresponden a la bolsa de aire del pasajero. Por otra parte el resto de su requerimiento es para una cita de servicio del sistema gas que sera programada para le dia MARTES 7:30 AM', 0),
(80, 62, 36, 1, 'Check VSC system y tripod izquierdo', '2020-09-21 07:30:00', -2, '2020-09-19 13:20:28', NULL, 'Revisión general', 1, 110, '', 0),
(81, 66, 37, 5, 'El vehiculo presenta falla al momento de acelerar (pistonea).', '2020-10-07 08:00:00', 1, '2020-10-02 21:37:39', NULL, 'Revisión general', 1, 40, '', 0),
(82, 67, 38, 3, 'Motor perdió fuerza al encender el Check', '2020-10-05 08:00:00', -1, '2020-10-04 22:34:25', NULL, 'Revisión general', 1, 150, '', 0),
(83, 71, 39, 2, 'Cambio de filtro y aceite.', '2020-10-20 08:30:00', -2, '2020-10-16 15:06:39', NULL, 'Mantenimiento períodico', 195, 201, 'Buenos dias, podria indicarnos en que fecha desea la cita', 0),
(84, 74, 42, 5, 'Mantenimiento regular, brincos entre primera y segunda (posible croché), sonido de maraqueo eventual por debajo (revisión de trípodes). El modelo real es Terios AWD pero no aparecía en la lista', '2020-11-03 07:30:00', 0, '2020-10-30 14:46:45', NULL, 'Mantenimiento períodico', 180, 201, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios_reclamos`
--

CREATE TABLE `comentarios_reclamos` (
  `id` int(11) NOT NULL,
  `id_reclamo` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `comentario` text DEFAULT NULL,
  `fec_com` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comentarios_reclamos`
--

INSERT INTO `comentarios_reclamos` (`id`, `id_reclamo`, `id_usuario`, `id_admin`, `comentario`, `fec_com`) VALUES
(17, 7, NULL, 1, 'Esta es otra prueba desde la compañía', '2020-01-21 14:18:14'),
(18, 10, 1, NULL, 'Comentario de prueba', '2020-06-25 12:01:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL,
  `nom_dep` varchar(50) DEFAULT NULL,
  `des_dep` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `nom_dep`, `des_dep`) VALUES
(1, 'Vehículos', ''),
(2, 'Repuestos', ''),
(3, 'Servicios', ''),
(4, 'ELP', ''),
(5, 'Otros', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fallas`
--

CREATE TABLE `fallas` (
  `id` int(11) NOT NULL,
  `falla` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `fallas`
--

INSERT INTO `fallas` (`id`, `falla`) VALUES
(1, 'Revisión según Kilometraje (Diagnóstico)'),
(20, 'Alineación'),
(30, 'Balanceo'),
(40, 'Solo Cambio de Aceite y Filtro'),
(50, 'Revisión de Frenos'),
(60, 'Diagnóstico Tren Delantero'),
(70, 'Diagnóstico A/Acondicionado'),
(80, 'Diagnóstico Recalentamiento de Motor'),
(90, 'Revisión Falla de Encendido'),
(100, 'Limpieza de Inyectores'),
(110, 'Diagnóstico de Tripoides'),
(120, 'Diagnóstico de Amortiguadores'),
(130, 'Revisar Fuga de Aceite de Motor'),
(140, 'Revisar Ruido de Rodamiento'),
(150, 'Revisar Luz Encendida en el Tablero'),
(160, 'Revisión y Diagnóstico de Embrague'),
(170, 'Revisión y Diagnóstico de Refrigerante'),
(180, 'Revisar Alternador'),
(190, 'Revisar Arranque'),
(200, 'Revisión y Diagnóstico Ruido en terreno Terrenal'),
(201, 'No aplica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kilometrajes`
--

CREATE TABLE `kilometrajes` (
  `id` int(11) NOT NULL,
  `kilometros` varchar(100) DEFAULT NULL,
  `tipo_kil` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `kilometrajes`
--

INSERT INTO `kilometrajes` (`id`, `kilometros`, `tipo_kil`) VALUES
(1, 'No aplica', ''),
(2, '1.000 con garantía', 'gar'),
(3, '10.000 con garantía', 'gar'),
(4, '1.000 sin garantía', 'per'),
(5, '5.000 Kilometros', 'per'),
(60, '10.000 Kilometros', 'per'),
(65, '15.000 Kilometros', 'per'),
(70, '20.000 Kilometros', 'per'),
(75, '25.000 Kilometros', 'per'),
(80, '30.000 Kilometros', 'per'),
(85, '35.000 Kilometros', 'per'),
(90, '40.000 Kilometros', 'per'),
(95, '45.000 Kilometros', 'per'),
(100, '50.000 Kilometros', 'per'),
(105, '55.000 Kilometros', 'per'),
(110, '60.000 Kilometros', 'per'),
(115, '65.000 Kilómetros', 'per'),
(120, '70.000 Kilómetros', 'per'),
(125, '75.000 Kilómetros', 'per'),
(130, '80.000 Kilómetros', 'per'),
(135, '85.000 Kilómetros', 'per'),
(140, '90.000 Kilómetros', 'per'),
(145, '95.000 Kilómetros', 'per'),
(150, '100.000 Kilómetros', 'per'),
(155, '105.000 Kilómetros', 'per'),
(160, '110.000 Kilómetros', 'per'),
(165, '115.000 Kilómetros', 'per'),
(170, '120.000 Kilómetros', 'per'),
(175, '125.000 Kilómetros', 'per'),
(180, '130.000 Kilómetros', 'per'),
(185, '135.000 Kilómetros', 'per'),
(190, '140.000 Kilómetros', 'per'),
(195, '145.000 Kilómetros', 'per'),
(200, '150.000 Kilómetros', 'per'),
(205, '155.000 Kilómetros', 'per'),
(210, '160.000 Kilómetros', 'per'),
(215, '165.000 Kilómetros', 'per'),
(220, '170.000 Kilómetros', 'per'),
(225, '175.000 Kilómetros', 'per'),
(230, '180.000 Kilómetros o Superior', 'per');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `misvehiculos`
--

CREATE TABLE `misvehiculos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_modelo` int(11) DEFAULT NULL,
  `placa` varchar(20) DEFAULT NULL,
  `serial1` varchar(40) DEFAULT NULL,
  `serial2` varchar(40) DEFAULT NULL,
  `serial3` varchar(40) DEFAULT NULL,
  `serial4` varchar(40) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `misvehiculos`
--

INSERT INTO `misvehiculos` (`id`, `id_usuario`, `id_modelo`, `placa`, `serial1`, `serial2`, `serial3`, `serial4`, `ano`) VALUES
(3, 1, 1, '0129312309', '123456789012345', 'azul', 'Automatico', '', 2000),
(4, 1, 1, '00012312', '129319239812312893128', 'verde', 'Automatico', '', 2000),
(5, 5, 2, 'jjwwee', '0123912391238123', NULL, NULL, NULL, 2001),
(6, 7, 6, 'AB258WN', '8AJHU3FS7K0360093', 'Blanco', 'Automatico', NULL, 2019),
(7, 7, 3, 'AF520TG', '8XBBA42E7DR825844', 'Azul', 'Automatico', NULL, 2013),
(8, 9, 3, 'AD754UA', '8XBBA42E3A7809633', 'GRIS', 'Automatico', NULL, 2010),
(9, 12, 1, '4654654654654', '457ase896734rfd', 'azul', 'Automatico', NULL, 2020),
(10, 13, 6, 'AB975MT', '8XAYU59G5KR001762', 'BLANCO', 'Automatico', NULL, 2019),
(11, 15, 2, 'MBF69B', '123456789012345', 'BEIGE', 'Automatico', NULL, 1998),
(12, 1, 1, 'MFL63A', '123456789012345', 'GRIS', 'Automatico', NULL, 2007),
(13, 6, 1, 'AA123AA', 'Jtdw13479j1438298', 'Azul ', 'Automatico', NULL, 2019),
(14, 22, 3, 'AG256HG', '8XBBA42E8DR828090', 'Purpura', 'Automatico', NULL, 2013),
(15, 25, 3, 'AH578MV', '8XBBA3HE8JR002434', 'BLANCO', 'Automatico', NULL, 2018),
(16, 29, 8, 'TAS01W', '9FH11UJ9089019397', 'GRIS', 'Manual', NULL, 2008),
(17, 32, 1, 'GDO41B', 'JTDKW923372007302', 'Negro', 'Automatico', NULL, 2007),
(18, 33, 1, 'AB592MM', 'JTDKW923475072612', 'azul metalico', 'Automatico', NULL, 2007),
(19, 40, 3, 'AA272ZM', '8xbba42e997802018', 'beige', 'Manual', NULL, 2009),
(20, 41, 1, 'AA799MW', 'JTDJW923X75036127', 'Negro', 'Automatico', NULL, 2007),
(21, 43, 3, 'AC866SE', '8XBBA3HE1HR002334', 'PLATA GALAXY', 'Automatico', NULL, 2017),
(22, 45, 3, 'MET67U', '8XBBA42E8A7812088', 'Verde Manglar', 'Automatico', NULL, 2007),
(23, 46, 6, 'AF761TG', '8XAYU59GXDR015207', 'GRIS', 'Automatico', NULL, 2013),
(24, 48, 6, 'AA024MG', 'MR0YU59G188001192', 'gris', 'Automatico', NULL, 2008),
(25, 48, 6, 'AJ221WA', '8XAYU59G4ER016726', 'gris', 'Automatico', NULL, 2014),
(26, 49, 3, 'AG587UA', '8XBBA42E6CR823713', 'Beige', 'Automatico', NULL, 2012),
(27, 36, 3, 'AA737GW', '8XA53ZEC179514206', 'BEIGE', 'Manual', NULL, 2007),
(28, 36, 3, 'AD661XA', '8XA53ZEC169512385', 'BEIGE', 'Manual', NULL, 2006),
(29, 53, 3, 'AD237VA', '8XBBA42E9A7811466', 'Azul', 'Automatico', NULL, 2010),
(30, 56, 1, '34534T5345', '34534587978978978978978', 'azul', 'Automatico', NULL, 4587),
(31, 57, 6, 'AJ924SM', '8XAYU59GXKR001742', 'PLATA', 'Automatico', NULL, 2019),
(32, 57, 6, 'AG737UA', '8XAYU5969CR013169', 'AZUL', 'Automatico', NULL, 2012),
(33, 57, 3, 'AF801YM', '8XBBA8HE1FR001758', 'PLATA', 'Automatico', NULL, 2015),
(34, 58, 2, 'AD043CE', '4T1B11HK9JU124131', 'PLATA', 'Automatico', NULL, 2018),
(35, 59, 3, 'AK767DA', '8XBBA42E8ER830391', 'DORADO', 'Automatico', NULL, 2014),
(36, 62, 2, 'AA017UO', 'JTNBK40K583032931', 'GRIS', 'Automatico', NULL, 2008),
(37, 66, 8, 'A17AH9L', '8XA33ZV25B9010823', 'Plata', 'Automatico', NULL, 2011),
(38, 67, 1, 'AA522CL', '1701042541530152TY177102', 'AZUL', 'Automatico', NULL, 2008),
(39, 71, 3, 'AEA34Z', '8XA53AEB225010931', 'Plata', 'Automatico', NULL, 2002),
(40, 70, 3, 'AI484RA', '8XA53AEB122022360', 'Verde', 'Automatico', NULL, 2002),
(41, 73, 3, 'AC463FD', 'BXA53ZEC169510742', 'BLANCO', 'Manual', NULL, 2006),
(42, 74, 1, 'LAO27G', '8XAJ102G049500468', 'GRIS', 'Manual', NULL, 2004);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelos`
--

CREATE TABLE `modelos` (
  `id` int(11) NOT NULL,
  `modelo` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modelos`
--

INSERT INTO `modelos` (`id`, `modelo`) VALUES
(1, 'COROLLA'),
(2, 'CAMRY'),
(5, 'Yaris'),
(6, 'Fortuner'),
(7, 'hiace'),
(8, 'Hilux'),
(10, 'LAND CRUISER');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelos_citas`
--

CREATE TABLE `modelos_citas` (
  `id` int(11) NOT NULL,
  `modelo` text DEFAULT NULL,
  `transmision` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modelos_citas`
--

INSERT INTO `modelos_citas` (`id`, `modelo`, `transmision`) VALUES
(1, 'Yaris', 'Automatica'),
(2, 'Camry', 'Manual'),
(3, 'Corolla', 'Automatica'),
(5, '4Runner', 'Automatica'),
(6, 'Fortuner', 'Automática'),
(7, 'Hiace', NULL),
(8, 'Hilux', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `descripcion` longtext DEFAULT NULL,
  `img` text DEFAULT NULL,
  `fec_reg_noti` date DEFAULT NULL,
  `est_noti` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `descripcion`, `img`, `fec_reg_noti`, `est_noti`) VALUES
(4, 'Conoce nuestros servicios', 'Conoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios a', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_08_27_11_27_41mantenimiento.jpg', '2019-08-27', 0),
(5, 'Latoneria y pintura una de nuestras especialidades', 'Latoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidades', 'https://exiauto.com/static/img/files/2019_09_11_12_47_062019_08_27_11_27_22latoneria.jpg', '2019-08-27', 0),
(7, 'La Ruta del Llamado Toyota', 'Exiauto Forma parte de La Ruta del Llamado Toyota.\r\nAlgunas Bolsas de Aire pudiesen presentar un defecto en el inflador, que podr&iacute;a ocasionar graves lesiones. Ven a Exiauto y reemplazaremos el inflador por uno nuevo; es totalmente gratis, r&aacute;pido y f&aacute;cil.\r\n\r\nMientras aplicamos el Llamado, podr&aacute;s disfrutar de la exhibici&oacute;n de veh&iacute;culos, foodtrucks, m&uacute;sica y un &aacute;rea exclusiva para los m&aacute;s peque&ntilde;os y seguridad.\r\n\r\nviernes y s&aacute;bado: 8:00 am - 5:00 pm\r\ndomingo: 9:00 am - 3:00 pm \r\n\r\n&iexcl;No te arriesgues! Ven a La Ruta ????.', 'https://exiauto.com/static/img/files/2019_09_11_12_46_342019_09_10_13_13_21Ruta_LLamado.jpg', '2019-09-10', 1),
(8, 'El Toyota Yaris Hatchback 2020 para Japón obtiene AWD, plataforma completamente nueva', '\r\nEl Toyota Yaris est&aacute; muerto en Estados Unidos, pero vivir&aacute; en todo el resto del mundo. Todav&iacute;a obtenemos la versi&oacute;n angular, basada en Mazda, que est&aacute; identificada como Toyota, pero esta nueva que obtendr&aacute; el resto del mundo es un poco m&aacute;s completa y casi completamente diferente. El nuevo Yaris en el extranjero se basa en la plataforma de Nueva Arquitectura Global (TNGA) de Toyota y cuenta con tres opciones de tren motriz, mientras que la versi&oacute;n que tenemos en Estados Unidos solo tiene una.\r\nLa plataforma TNGA se usa por primera vez en un autom&oacute;vil subcompacto, y Toyota dice que a partir de ahora basar&aacute;n todos sus autom&oacute;viles subcompactos en esta plataforma. El uso de esta plataforma ayuda a darle al Yaris un poco m&aacute;s de espacio en la cabina junto con una suspensi&oacute;n actualizada y un cuerpo que es 100 libras m&aacute;s ligero.\r\n \r\nEl 1.5 litros de tres cilindros est&aacute; disponible con un nuevo sistema h&iacute;brido que proviene de los m&aacute;s grandes que se encuentran en el Corolla, RAV4 y Camry. Se ofrece con el sistema el&eacute;ctrico de tracci&oacute;n total E-Four de Toyota, otra primicia para los autos subcompactos de Toyota. Toyota a&uacute;n no ha reclamado los n&uacute;meros de potencia, pero s&iacute; dice que el h&iacute;brido tiene un 15 por ciento m&aacute;s de potencia combinada y un aumento de m&aacute;s del 20 por ciento en la eficiencia del combustible. Tambi&eacute;n hay disponible una versi&oacute;n a gasolina del motor de 1.5 litros y uno de tres cilindros de 1.0 litro. En los Estados Unidos, el sed&aacute;n Yaris con sede en Mazda viene de serie con una transmisi&oacute;n manual, aunque el hatchback solo est&aacute; disponible con una transmisi&oacute;n autom&aacute;tica de seis velocidades. En la nueva escotilla Yaris que no obtendremos, se ofrece un manual de seis velocidades con el motor de gasolina de 1.5 litros. Una transmisi&oacute;n continua de cambio continuo con tracci&oacute;n delantera o tracci&oacute;n total est&aacute; disponible para el 1.5 litros, y un CVT con tracci&oacute;n delantera est&aacute; disponible para el 1.0 litro.\r\nLa bater&iacute;a de hidruro de n&iacute;quel-metal que se encontraba en el Yaris de anta&ntilde;o ha sido reemplazada por una bater&iacute;a h&iacute;brida de iones de litio, que seg&uacute;n Toyota puede ayudar a aumentar la aceleraci&oacute;n. La nueva bater&iacute;a tambi&eacute;n es un 27 por ciento m&aacute;s ligera que la que reemplaza.\r\nToyota Safety Sense es est&aacute;ndar en el nuevo Yaris, presentando el debut de un sistema de asistencia de estacionamiento, Advanced Park, que es nuevo para Toyota, y un sistema de detecci&oacute;n de autos que se aproxima, otro primero; Tambi&eacute;n incluye las caracter&iacute;sticas de seguridad activa esperadas, como control de crucero adaptativo y asistencia de mantenimiento de carril. Una pantalla frontal de color de 10 pulgadas tambi&eacute;n es est&aacute;ndar.\r\nEl Toyota Yaris 2020 estar&aacute; en exhibici&oacute;n en el Auto Show de Tokio esta semana y a la venta en Jap&oacute;n a partir de mediados de febrero de 2020.\r\n\r\n', 'https://exiauto.com/static/img/files/2019_10_24_11_28_14toyota2.jpg', '2019-10-24', 1),
(9, 'Procedimientos que debes realizar con profesionales como Exiauto, y no por tu cuenta. ', 'Hay procedimientos que pueden observarse como sencillos para el usuario, pero por su real dificultad o consecuencias que puede desencadenar en tu veh&iacute;culo, te recomendamos hacerlas con un profesional:\r\n\r\n1. Cambiar el aceite y su filtro: En esta operaci&oacute;n se observa como muy simple para los usuarios, sin embargo hay que tener en cuenta el aceite adecuado para tu veh&iacute;culo, el estatus, y la gesti&oacute;n de los residuos como marca la ley.\r\n\r\n2. Cambiar las pastillas y discos de freno: Hay que tener en cuenta que los frenos son unos de los elementos de seguridad m&aacute;s importantes del vehiculo, por eso este cambio debe realizarlo un profesional. Adem&aacute;s de los conocimientos, hay que tener todos las herramientas necesarias.\r\n\r\n3. Cambiar la distribuci&oacute;n (correa o kit) y la bomba de agua: Se trata de una operaci&oacute;n m&aacute;s compleja, que de no realizarse adecuadamente, puede da&ntilde;ar el motor de tu veh&iacute;culo.\r\n\r\n4. Recargar el aire acondicionado:  Existen varios tipos de gas y utilizar uno que no sea el adecuado puede resultar peligroso. Hay que tener en cuenta que el gas es inflamable y su manipulaci&oacute;n es muy peligrosa.\r\n\r\n5. Cambiar l&iacute;quido de frenos: Es una tarea que puede resultar complicada y que un profesional puede realizar sin problemas.', 'https://exiauto.com/static/img/files/2019_11_26_09_56_07toyota_VOLANTE.jpg', '2019-11-26', 1),
(10, '¿Conoces la nueva Toyota Hilux GR Sport?', 'Toyota presenta la Hilux GR Sport, una nueva versi&oacute;n de Toyota GAZOO Racing.\r\nLa nueva Hilux GR Sport suma tambi&eacute;n equipamiento funcional pensado para brindarle una experiencia superadora al conductor off-road.\r\nEquipada con un motor V6 naftero de 238 CV de potencia m&aacute;xima, una caja autom&aacute;tica de 6 velocidades.\r\nEste conjunto entrega una aceleraci&oacute;n m&aacute;s en&eacute;rgica, con reacciones m&aacute;s r&aacute;pidas, brind&aacute;ndole al veh&iacute;culo un acentuado car&aacute;cter deportivo que resulta en una conducci&oacute;n m&aacute;s emocionante.\r\n', 'https://exiauto.com/static/img/files/2020_02_04_13_55_34gr_sport-3.jpg', '2020-02-04', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reclamos`
--

CREATE TABLE `reclamos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fec_reg_rec` datetime DEFAULT NULL,
  `mensaje` text DEFAULT NULL,
  `est_rec` int(1) DEFAULT NULL,
  `tipo` varchar(1) DEFAULT NULL,
  `id_departamento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `reclamos`
--

INSERT INTO `reclamos` (`id`, `id_usuario`, `fec_reg_rec`, `mensaje`, `est_rec`, `tipo`, `id_departamento`) VALUES
(7, 15, '2020-01-21 14:14:50', 'Prueba de sugerencia desde los desarrolladores', 1, 'S', 5),
(8, 6, '2020-01-27 08:18:44', 'MI VEHICULO QUEDO SUCIO ', 1, 'R', 3),
(10, 1, '2020-06-25 11:58:33', 'Prueba desde Plade Company.', 1, 'S', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repuestos`
--

CREATE TABLE `repuestos` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `nom_rep` varchar(100) DEFAULT NULL,
  `des_rep` text DEFAULT NULL,
  `ano_rep` int(11) DEFAULT NULL,
  `pre_rep` float DEFAULT NULL,
  `cam_1` varchar(100) DEFAULT NULL,
  `cam_2` varchar(100) DEFAULT NULL,
  `cam_3` varchar(100) DEFAULT NULL,
  `img1` text DEFAULT NULL,
  `img2` text DEFAULT NULL,
  `img3` text DEFAULT NULL,
  `img4` text DEFAULT NULL,
  `img5` text DEFAULT NULL,
  `est_rep` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud_repuestos`
--

CREATE TABLE `solicitud_repuestos` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `ape` varchar(30) DEFAULT NULL,
  `tel` varchar(15) DEFAULT NULL,
  `cor` varchar(100) DEFAULT NULL,
  `repuesto` text DEFAULT NULL,
  `fec_reg_sol` datetime DEFAULT NULL,
  `estatus` int(1) DEFAULT NULL,
  `fec_pro_sol` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `solicitud_repuestos`
--

INSERT INTO `solicitud_repuestos` (`id`, `nom`, `ape`, `tel`, `cor`, `repuesto`, `fec_reg_sol`, `estatus`, `fec_pro_sol`) VALUES
(1, 'carlos', 'castellanos', '04120545738', 'castellanos@pladecompany.com', 'Esta es una prueba de las solicitudes de repuestos desde la pagina web.', '2020-02-18 16:32:08', 1, NULL),
(2, 'Andrey ', 'Colmenares ', '04125747451 ', 'andrycolmenares24@hotmail.com ', 'Espirales delantero y trasero de una hilux doble cabina 2.7 año 2007 ', '2020-02-19 11:45:24', 1, NULL),
(3, 'Alys', 'Alfonzo ', '04148080344', 'alfonzoalys@gmail.com ', 'Control de mando eleva vidrios electrónico de puerta de piloto Yaris Belta año 2007 ', '2020-02-19 14:37:56', 1, NULL),
(4, 'Alys', 'Alfonzo ', '04148080344', 'alfonzoalys@gmail.com ', 'Control de mando eleva vidrios electrónico de puerta de piloto Yaris Belta año 2007 ', '2020-02-19 14:38:04', 1, NULL),
(5, 'RAFAEL', 'ALVAREZ', '04122862541', 'info@espacioconvalor.com', 'REGULADOR DE ALTERNADOR 4 PINES PARA YARIS 3 PUERTAS 2007 ', '2020-02-20 14:08:27', 1, NULL),
(6, 'JOSEFINA ', 'SANCHEZ ', '04142209965', 'josefinasanchezoyuela@gmail.com', 'El flotante de la Bomba de Gasolina , Camry 1998', '2020-02-20 15:30:35', 1, NULL),
(7, 'Daniela', 'Egui', '04241760238', 'eguirubiodaniela@yahoo.com.ve', 'Filtros de aceite, gasolina y aire para Toyota Corolla 2010', '2020-02-26 09:00:38', 1, NULL),
(8, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL),
(9, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL),
(10, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL),
(11, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL),
(12, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL),
(13, 'Rafael', 'Toro', '04149213982', 'tororafael@gmail.com', 'Para previa 2009. NO Smart\nBases de motor y caja. Todas.\nCorrea Unica y tensor.\nEstopera trasera de sigueñal.\nFiltro Aceite.', '2020-02-27 08:54:04', 1, NULL),
(14, 'Fabian', 'Alvarez', '04143226012', 'fabian.alvarez@aig-ca.com.ve', 'Solicitó la rolinera de aguja del transfer para hilux 2.7 año 2008, 2.7 4x4', '2020-02-27 09:59:46', 1, NULL),
(15, 'franklin ', 'rivero', '04141876276', 'frgofc@gmail.com', 'buenos días \nando buscando las 4 gomas de los tripoides para un toyota corolla 2009 GLI si me podrían ayudar sería buenísimo y muchas gracias ', '2020-02-28 09:23:42', 1, NULL),
(16, 'José  Alejan', 'Pinto Arno', '04242281302', 'Josepintoa@gmail.com', 'Guía trasera izquierda, del parachoque de una 4 Runner, 2008', '2020-02-28 11:07:50', 1, NULL),
(17, 'Omar ', 'rios ', '0412 8779734', 'Omarsk_8@hotmail.com', 'kit cadena de tiempo yaris belta motor 1.5', '2020-02-28 14:22:42', 1, NULL),
(18, 'Ramiro', 'Aliaga', '04143101220', 'ramiroaliaga1973@hotmail.com', 'Necesito para una Merú 2008 las barras tensoras superiores de la transmision trasera, son las barras cortas(las superiores) creo que su nro de pieza es 48710 ', '2020-03-02 14:36:38', 1, NULL),
(19, 'DOMINGO', 'MIJARES', '040142319078', 'domingomijares27@gmail.com', 'AMORTIGUADORES DELANTEROS COROLLA 2010.\nGRACIAS ', '2020-03-03 16:00:08', 1, NULL),
(20, 'Maikel ', 'Tarezian ', '04241877396', 'maikelter13@hotmail.com', 'Buenas noches disponen \nDe los siguientes repuestos originales para Yaris 2007\n\n*Termostato\n*Rodamiento delantero imantados con ABS \n\nGracias ', '2020-03-09 22:41:26', 1, NULL),
(21, 'Cesar', 'Barrios', '04123079234', 'cbarrios58@gmail.com', 'Correa de alternador para Yaris 2002 1.3 sincrónico ', '2020-03-11 15:49:39', 1, NULL),
(22, 'Tomás ', 'Blohm', '0414 3174818', 'tfblohm61@yahoo.es', 'Busco motor de arranque para FJ40 1984', '2020-04-07 13:34:43', 1, NULL),
(23, 'Dario', 'Brillembourg', '04141321885', 'fb.dario@gmail.com', 'Fusible Alternador 100amp TOYOTA PRADO 2004', '2020-04-18 06:30:57', 1, NULL),
(24, 'Jose', 'MORA', '04149022565', 'jamv2507@gmail.com', 'Necesito cotizar un repuesto. Modalidades de pago. Repuesto \nAlternador Toyota Corolla 2001. Motor 1.6', '2020-04-28 18:06:48', 1, NULL),
(25, 'Ma Aurora', 'PANDO', '04142259209', 'yoyazulita@hotmail.com', 'Buen día,\nTienen las pastillas de frenos para Corolla 2005 y precio? Gracias', '2020-05-26 18:43:15', 1, NULL),
(26, 'Gilcar', 'Revolledo', '04242752247', 'gilcar.revolledo@gmail.com', 'Necesito alternador original para un Fj40 ano 73-78. o alguna referencia donde conseguirlo. Gracias', '2020-06-01 11:00:39', 1, NULL),
(27, 'irvin', 'gudiño', '04265198512', 'irvin.gudino3@gmail.com', 'Buenos dias para cotizar las cuatro bases del motor para el Corolla 2010.\nGracias', '2020-06-03 11:31:39', 1, NULL),
(28, 'irvin', 'gudiño', '04265198512', 'irvin.gudino3@gmail.com', 'Buenos dias, para cotizar las cuatro bases del motor para corolla automatico 2010.\ngracias.', '2020-06-03 11:37:04', 1, NULL),
(29, 'Raul', 'Martinez', '0412 260 22 72', 'ramartinez005@gmail.com', 'Bomba de freno de la camioneta Van  Hiace ', '2020-06-07 21:25:28', 1, NULL),
(30, 'Ma Aurora ', 'Pando', '04142259209', 'yoyazulita@hotmail.com', 'Buen día, tienen pastillas de frenos para Corolla 2005? ', '2020-06-08 18:28:40', 1, NULL),
(31, 'Ricardo', 'Quilen', '0424-1469539', 'rquilen@gmail.com', 'Buenas noches.\nSon dos repuestos:\nYARIS AÑO 2000: Sensor de temperatura que activa electro ventilador.\nCOROLLA AÑO 2001: Flotante tanque de gasolina ', '2020-06-09 19:55:12', 1, NULL),
(32, 'ANTONIO', 'MORALES', '04143311021', 'antoniomoraless@hotmail.com', 'MOTOR DEL ELECTROVENTILADOR DEL RADIADOR\nTOYOTA COROLLA 18 AUTOMATICO AÑO 1997\n', '2020-06-16 12:09:22', 1, NULL),
(33, 'Carlis ', 'Rivas', '04242093227', 'crivas.mbglegal@gmail.com', 'Conjunto extremo cremallera (rotula)\nBarra acoplamiento derecha\nBarra acoplamiento izquierda\nAmortiguadores delanteros\nZapata derecha #1\nZapata izquierda #1\nBuje brazo contro inferior tras. der/izq\nZapata derecha #2\nZapata izquierda #2\nBuje barra estabilizadora \nCrucetas\nAmortiguadores traseros der/iz', '2020-06-17 10:43:31', 1, NULL),
(34, 'ramon', 'low', '5648292', 'ramonlow13@gmail.com', 'manguera bomba dirección hidráulica Autana 2002 la pequeña de abajo ', '2020-06-17 14:48:02', 1, NULL),
(35, 'Jose ', 'Martinez', '04166871137', 'jose.martinez3777@gmail.com', '1 Inyector y 2 conectores para Toyota Yaris año 2000', '2020-06-18 18:24:14', 1, NULL),
(36, 'hector ', 'tortolero', '0414-3390040', 'hector.tor.52@gmail.com', 'moldura antigoteo de techo yaris belta 2007 nr. de parte 75552-52140 75551-52160 o 75552-130 mide entre 1.49 y 1.50 mts largo ', '2020-06-30 10:13:03', 1, NULL),
(37, 'María A', 'Castillo', '04243230886', 'mariasancastillo@hotmail.com', 'Distribución Toyota Starlet Año 1998 motor 2E', '2020-07-07 14:28:04', 1, NULL),
(38, 'María a', 'Castillo', 'O4243230886', 'mariasancastillo@hotmail.com', 'Distribución Toyota Starlet Año 1997 motor 2E', '2020-07-07 14:32:07', 1, NULL),
(39, 'Luis ', 'Pulido ', '0416.3119839', 'Luis.pulidop@hotmail.com ', 'Regulador de alternador Toyota fortuner año 2008. Código pieza 126000-3720.', '2020-07-24 22:12:57', 1, NULL),
(40, 'Perla ', 'Martinez', '0414-236.67.30', 'pmartinez@unirent.com.ve', 'Manguera inferior (Nro. 2) de radiador para Hilux 4.0 año 2017 ensamblada en Venezuela, tengo 3 número de parte: 16572-0C070, 16572-0P030 y 16572-0C090\n\nGracias de antemano,', '2020-07-30 10:08:22', 1, NULL),
(41, 'gabriel', 'gonzalez', '04241644265', 'gagb248@gmail.com', 'radio original toyota para un corolla 2012\nel modelo que viene con Bluetooth \npor favor contactar via email', '2020-08-03 01:10:04', 1, NULL),
(42, 'Nila', 'Yáñez ', '04123180391', 'Nyanezg03@gmail.com ', 'Bombillo luz baja corolla 2011', '2020-08-03 14:26:11', 1, NULL),
(43, 'juan', 'marques', '04143204988', 'jmarques@meister.com.ve', 'Buenos días, necesito un gato y los accesorios de una hilux', '2020-08-11 11:02:52', 1, NULL),
(44, 'Ninsoka Caro', 'Velasquez', '04166282325', 'carolavelco@yahoo.com', 'Pila original bomba de gasolina Corolla 2010.\nCodigo 23220-22181. Gracias.', '2020-08-19 10:26:45', 1, NULL),
(45, 'Cesar', 'Rodriguez', '04122004203', 'jarias123@gmail.com', 'Pastillas de freno delanteras y traseras para CAMRY SE 2017? Gracias', '2020-08-20 17:41:24', 1, NULL),
(46, 'Robert ', 'Rodriguez ', '04241260366', 'robertluis2005@gmail.com ', 'Para una Meru 2008\nKit de tiempo\nJuego de empacaduras \nAnillos Std\nCorrea de Alternador\nCables de bujía\nBujias\nFiltro de aceite\nRefrigerante', '2020-08-21 11:55:58', 1, NULL),
(47, 'Yoniray', 'Zambrano', '04125631217', 'yoniray@gmail.com', 'Manillas Internas Corolla 2002 ', '2020-08-25 13:12:56', 1, NULL),
(48, 'Gabriela ', 'Baute', '04141394576', 'gabriela31_6@hotmail.com ', 'Bomba de gasolina para runner 2008 el número de pieza es 77020 - 35071\n       Gracias!!', '2020-08-26 14:40:51', 1, NULL),
(49, 'jose luis', 'Perisse', '0414 1800198', 'jose.luis.perisse@gmail.com', 'Requiero\nMesetas con sus bujes para Toyota Previa\nnecesito disponibilidad y costo', '2020-09-04 12:00:34', 1, NULL),
(50, 'Antonio', 'Ziegler', '04123268056', 'antonioziegler@hotmail.com', 'cajetín de dirección Meru y croche completo Meru.', '2020-09-07 17:54:24', 1, NULL),
(51, 'frrr', 'fffff', '12424523658', 'dfgdxrgx@glrdjg.com', '<sukfhsiudfh', '2020-09-08 12:07:07', 1, NULL),
(52, 'María ', 'MONTILLA GON', '04143116390', 'marelimon@gmail.com', 'Buenas tardes: \nNecesito los siguientes repuestos para un New sensation 2003\n-lápices (2)\n- amortiguadores delanteros (2) y traseros(2)\n-Bujes grandes (2) para meseta delanteros\n\nSaludos', '2020-09-09 15:58:04', 1, NULL),
(53, 'Jenny', 'Ramírez ', '04242582123', 'terranjenny@hotmail.com', 'Requiero costo de las pastillas delanteras y traseras de una 4runner año 2015.\n\nY costo de instalación de mano de obra por instalación de pastillas.\n\nGracias', '2020-09-13 16:36:57', 1, NULL),
(54, 'Agustín ', 'Ascanio', '0424.2050989', 'agustinascanio@gmail.com', 'Clutch Terios 2005. (Plato. collarín y disco)', '2020-09-21 09:02:23', 1, NULL),
(55, 'Adolfo', 'Salazar', '04149612313', 'aesb2000@gmail.com', 'Stop trasero derecho, Terios Bego 2008\nFiltro de aire\nFiltro de gasolina', '2020-09-23 20:45:55', 1, NULL),
(56, 'Antonio', 'Guercio', '04143203647', 'frasavini@gmail.com', 'Compresor A/A Previa 2007', '2020-09-24 14:48:21', 1, NULL),
(57, 'Rafael ', 'Liscano', '0414 5940534', 'rliscanov@gmail.com', 'Motor de arranque de Corolla automático año modelo 1998 motor 1.8 serial # AE1029509500\nFavor informar si tienen en existencia y precio del mismo. este repuesto lo requiero con urgencia.\nGracias, por la ayuda.\nRafael Liscano', '2020-09-27 19:25:27', 1, NULL),
(58, 'yosselys', 'pinto', '04263106785', 'yosman99@hotmail.com', 'concha de biela con cuñas 0,10\nconcha de bancada 0,1 con cuñas y otras sin cuñas es decir 5 con cuñas y 5 sin cuñas\nbomba de aceite\nkit de cadena de tiempo\nbases de  motor\nterios año 2007 motor 1,3 sincronica 4X4', '2020-09-29 16:32:54', 1, NULL),
(59, 'marco', 'fiorilli', '04241598811', 'mfiorilli@ronava.com', 'flotante gasolina Y BUJIAS DEL  Baby cAMTY 1.6 CARBURADO.', '2020-09-30 10:26:26', 1, NULL),
(60, 'Ingrid', 'Angelastro ', '04123059458', 'Ingridangelastro1@gmail.com', 'Frenos delanteros previa 2007', '2020-10-07 11:45:33', 1, NULL),
(61, 'Jhon', 'Montes de Oc', '0414 1237802', 'montesj_07@hotmail.com', 'Bases yaris sport automático 2008\nMotor trasera\nMotor derecha\nCaja izquierda\n', '2020-10-09 00:03:48', 1, NULL),
(62, 'CARLOS', 'FIGUEROA', '0414 2404806', 'carfi57@gmail.com', 'Buenos días, me interesa conocer los precios de los siguientes repuestos y, al mismo tiempo, saber si ustedes aceptan pagos via Zelle\nGracias por su atención\nToyota Corolla 1.8 año 2000 automático\n-- Kit de empacaduras del motor\n-- Kit correa de tiempos (tensor, patin y correas)\n-- Bomba de aceite marca Aisin original\n', '2020-10-16 11:05:16', 1, NULL),
(63, 'Mauro', 'Farabegoli', '04122396546', 'Farabem@gmail.com', 'Vacuum avance distribuidor\nToyota Corolla 1995 (Baby Camry) carburado. \nGracias ', '2020-10-21 11:15:18', 1, NULL),
(64, 'domingo ', 'utrera', '04125330267', 'angeldomingoutrera@gmail.com', 'árbol de leva (admisión ) para toyota previa 2009, motor cuatro cilindros', '2020-10-22 21:18:28', 1, NULL),
(65, 'Freddy', 'Peña', '04166382909', 'fpena02@gmail.com', 'Kits para el cajetín de de la dirección de la Meru 2008\nEstopera para la rueda trasera Izquierda de la Meru 2008', '2020-10-26 15:06:02', 1, NULL),
(66, 'Brigitte', 'Rodríguez', '04242726848', 'brigitter2809@gmail.com', 'Hola buenos días ! Quisiera saber si tienen disponible aspa de electroventilador de Terios Bego 2009.', '2020-10-29 09:03:57', 1, NULL),
(67, 'Brigitte', 'Rodríguez', '04242726848', 'brigitter2809@gmail.com', 'Hola buenos días ! Quisiera saber si tienen disponible aspa de electroventilador de Terios Bego 2009.', '2020-10-29 09:04:06', 1, NULL),
(68, 'Brigitte', 'Rodríguez', '04242726848', 'brigitter2809@gmail.com', 'Hola buenos días ! Quisiera saber si tienen disponible aspa de electroventilador de Terios Bego 2009.', '2020-10-29 09:04:11', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `ced_usu` varchar(15) DEFAULT NULL,
  `nom_usu` varchar(30) DEFAULT NULL,
  `ape_usu` varchar(35) DEFAULT NULL,
  `cor_usu` varchar(100) DEFAULT NULL,
  `tel_usu` varchar(15) DEFAULT NULL,
  `pas_usu` varchar(500) DEFAULT NULL,
  `fec_nac_usu` date DEFAULT NULL,
  `fec_reg_usu` datetime DEFAULT NULL,
  `emp_usu` varchar(100) DEFAULT NULL,
  `facebook` varchar(40) DEFAULT NULL,
  `instagram` varchar(40) DEFAULT NULL,
  `twitter` varchar(40) DEFAULT NULL,
  `img_usu` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `ced_usu`, `nom_usu`, `ape_usu`, `cor_usu`, `tel_usu`, `pas_usu`, `fec_nac_usu`, `fec_reg_usu`, `emp_usu`, `facebook`, `instagram`, `twitter`, `img_usu`) VALUES
(1, 'V-21024060', 'Carlos', 'Castellanos', 'castellanoscarlos15@gmail.com', '04120545738', '12345', '1993-10-15', '2019-12-23 13:50:45', '', 'casteca15', 'casteca15', 'casteca15', 'https://exiauto.com/static/img/files/2019_12_23_14_44_06plade-logo.jpg'),
(2, 'J-40087772', 'Ernesto', 'Fernandez', 'contacto@pladecompany.com', '04120545738', '12345', '1993-10-15', '2019-10-07 12:08:14', 'Plade Company', NULL, NULL, NULL, NULL),
(5, 'V-21160654', 'Daniela1', 'Manzanilla1', 'd1ani@gmail.com', '04123123123121', '12345', '1992-10-15', '2019-10-09 22:04:59', 'Plade1', NULL, NULL, NULL, NULL),
(6, 'V-24211288', 'giselle', 'cedeño', 'gcedeno@exiauto.com', '04120983189', '12397081', NULL, '2019-11-15 13:25:29', NULL, NULL, NULL, NULL, NULL),
(7, 'J-411673528', 'Miguel', 'Martínez', 'mmcastro76@gmail.com', '04143383179', 'mmc45162048f', NULL, '2019-11-18 08:46:56', 'Corporación Alekami, C. A.', NULL, NULL, NULL, NULL),
(8, 'V-12916924', 'Edicza Coromoto', 'Gonzalez Alvarez', 'gonzalezedicza59@gmail.com', '04142314762', 'diegoyyo2325', NULL, '2019-11-20 12:12:12', NULL, NULL, NULL, NULL, NULL),
(9, 'V-4275101', 'Susana', 'Belluardo', 'belluardosusana@gmail.com', '04142350506', '18081953', NULL, '2019-12-04 07:22:01', NULL, NULL, NULL, NULL, NULL),
(10, 'V-20077600', 'Juan ', 'Escalante', 'escalantejuan3592@gmail.com', '04242249892', '22042008', NULL, '2019-12-04 13:27:39', NULL, NULL, NULL, NULL, NULL),
(11, 'V-3720061', 'Elizabeth ', 'Cedeño ', 'elizabeth@fanjet.com', '04142005578', 'beth67.ec', NULL, '2019-12-05 07:54:13', NULL, NULL, NULL, NULL, NULL),
(12, 'V-14786421', 'carlos ', 'camacho ', 'carloscamachorivero@hotmail.com', '04143994798', '248605', NULL, '2019-12-05 10:56:19', NULL, NULL, NULL, NULL, NULL),
(13, 'V-15055273', 'Jose de los Santos', 'Oyarbe Ramoa', 'jose.oyarbe@gmail.com', '04241162838', 'Entrust12.', NULL, '2019-12-16 10:09:32', NULL, NULL, NULL, NULL, NULL),
(14, 'V-18027244-1', 'CESAR', 'MUÑOZ', 'cjmrios@gmail.com', '04142416486', 'cm135792468', NULL, '2019-12-18 11:45:24', NULL, NULL, NULL, NULL, NULL),
(15, 'V-11886187', 'miguel', 'farias', 'miguel_farias@hotmail.com', '04122680712', 'migfar', NULL, '2019-12-18 12:52:33', NULL, NULL, NULL, NULL, NULL),
(19, 'V-13972999', 'freddy johan', 'sanchez ramirez ', 'freddyjohan35@hotmail.com', '04166781773', '13972999/cordero', NULL, '2020-01-17 10:47:50', NULL, NULL, NULL, NULL, NULL),
(20, 'V-3789052', 'Beltran', 'Contreras', 'jorge.contreras@circuitolider.com', '04143741309', 'lider92', NULL, '2020-01-20 08:43:22', NULL, NULL, NULL, NULL, NULL),
(21, 'V-25716493', 'Ariagna', 'Pamelá', 'a.ariagna.p@gmail.com', '04129803941', '123456a', NULL, '2020-01-20 09:30:57', NULL, NULL, NULL, NULL, NULL),
(22, 'V-10337502', 'Rogelio', 'Carrillo', 'r.carrillo@outlook.com', '04143234138', '3S4loc8DhW7T', NULL, '2020-01-20 12:15:44', NULL, NULL, NULL, NULL, NULL),
(24, 'V-14989770', 'ramon', 'iglesias', 'riglesias47@gmail.com', '04122772982', '14989770', NULL, '2020-01-20 14:28:47', NULL, NULL, NULL, NULL, NULL),
(25, 'V-19371327', 'Barbara', 'Bautista Morales', 'blbautistam@gmail.com', '04149107439', 'anacleta232', NULL, '2020-01-21 09:45:21', NULL, NULL, NULL, NULL, NULL),
(26, 'V-7092221', 'CLARA LUCIA', 'VENTO MENDOZA', 'claraluciavento@gmail.com', '04166244745', 'KYRO2019', '1967-02-20', '2020-01-24 08:32:25', '', '', '@claraluciavento', '', NULL),
(27, 'V-3859941', 'edgar ', 'naranjo', 'enaranjo.tranred@gmail.com', '04141139773', 'aleeli01', NULL, '2020-01-24 09:47:48', NULL, NULL, NULL, NULL, NULL),
(29, 'V-10542478', 'Jesus', 'Salazar', 'chui.x3@gmail.com', '04166080004', 'Calabaza1', NULL, '2020-01-24 10:41:53', NULL, 'chuix3', 'chui.x3', '@jsalazarx01', NULL),
(30, 'V-9882633', 'gretchen', 'lauria', 'gretchenlauria@hotmail.com', '04143246260', '031269', NULL, '2020-01-29 15:41:27', NULL, NULL, NULL, NULL, NULL),
(31, 'V-6557658', 'GLADYS', 'TENORIO VALERA', 'GLADYSTENORIOV@GMAIL.COM', '04122638249', '12311121516', NULL, '2020-02-05 16:18:04', NULL, NULL, NULL, NULL, NULL),
(32, 'V-2091177', 'Maria Josefina', 'Gonzalez de Argento', 'mjargento@gmail.com', '04242002520', 'westminster20', NULL, '2020-02-06 12:31:46', NULL, NULL, NULL, NULL, NULL),
(33, 'V-4120287', 'Elizabeth', 'Rodriguez', 'eliro91@gmail.com', '04122156281', 'laguna8691', NULL, '2020-02-13 12:13:29', NULL, NULL, NULL, NULL, NULL),
(34, 'V-5003059', 'Marcos', 'Schnee', 'schneemarcos@gmail.com', '04241716352', 'mda67v', NULL, '2020-02-18 10:41:13', NULL, NULL, NULL, NULL, NULL),
(35, 'V-3176946', 'Carlos ', 'Cosson', 'luiscosson@gmail.com', '04166209666', 'abril2020', NULL, '2020-02-19 11:14:58', NULL, NULL, NULL, NULL, NULL),
(36, 'V-6867409', 'JUAN CARLOS', 'PARDO PISANI', 'jcpp2000@yahoo.com', '04241775803', 'fabi2403', '1967-02-07', '2020-07-15 09:22:50', '', '', '', '', NULL),
(37, 'V-12410746', 'robert', 'alvarez', 'roberthalvarez16@gmail.com', '04141349437', 'rob12410746', NULL, '2020-02-20 22:21:09', NULL, 'Roberthalvarez@hotmail.com', NULL, NULL, NULL),
(38, 'J-080271084', 'Jose', 'Diaz', 'jdiaz@disteinsa.com', '04148232980', 'disteinsa', NULL, '2020-02-26 10:09:42', 'Disteinsa, C.A.', NULL, NULL, NULL, NULL),
(39, 'V-6346955', 'Vicente', 'Adriani', 'vadriani10@gmail.com', '04241222827', '21267303', NULL, '2020-02-26 10:52:23', NULL, NULL, NULL, NULL, NULL),
(40, 'V-12544268', 'Azael', 'Camacho', 'azaelcamacho66@gmail.com', '04141092840', 'ac12544268', NULL, '2020-02-26 11:28:03', NULL, NULL, NULL, NULL, NULL),
(41, 'V-18954419', 'Karina', 'Licitra', 'karinalicitra@gmail.com', '04122244780', 'panini', NULL, '2020-02-27 10:23:40', NULL, NULL, NULL, NULL, NULL),
(42, 'V-3187305', 'Vicente', 'Antonorsi', 'administracion@anilarquitectura.com', '04142462560', '3187305', NULL, '2020-02-28 08:55:28', NULL, NULL, NULL, NULL, NULL),
(43, 'V-16007178', 'ALFREDO', 'MACHADO', 'machado.krueger@gmail.com', '04242489792', 'Aemk2308', NULL, '2020-02-29 11:56:39', NULL, NULL, NULL, NULL, NULL),
(44, 'E-81944834', 'vilma', 'massaro de dos santos', 'cristinamasaro@gmail.com', '04242381710', '4358556', '1957-03-15', '2020-06-10 07:32:01', '', '', '', '', NULL),
(45, 'V-6146495', 'Juan Carlos', 'Canga Linares', 'jcanga@yahoo.com', '04122354658', 'insi3108', NULL, '2020-03-10 09:29:52', NULL, NULL, NULL, NULL, NULL),
(46, 'V-14021958', 'Gabriel', 'Daza', 'gdaza@pm.com', '04242124175', '290280', NULL, '2020-03-11 10:15:53', NULL, NULL, NULL, NULL, NULL),
(47, 'V-11738714', 'Manuel Felipe', 'Rivas Dayan', 'manuel.rivas@gmail.com', '04141229938', '#1501Mfrd', NULL, '2020-03-11 17:31:38', NULL, NULL, '@mrivasdayan', NULL, NULL),
(48, 'V-3388800', 'Carlos', 'Fernandez', 'carferlan@gmail.com', '04122352798', 'solrac01', NULL, '2020-03-13 12:08:37', NULL, NULL, NULL, NULL, NULL),
(49, 'V-12453458', 'Lenin', 'Pena', 'leninp57@gmail.com', '04122329748', 'MFT74B.', NULL, '2020-05-05 12:21:47', NULL, NULL, NULL, NULL, NULL),
(50, 'V-11010772', 'Irvin', 'Gudiño', 'irvin.gudino3@gmail.com', '04265198512', '070772114', NULL, '2020-06-03 11:27:47', NULL, NULL, '@irvin.gudino3', '@vegudino', NULL),
(51, 'V-23533646', 'edgar', 'zapata', 'zapataedgar967@gmail.com', '04148585145', 'carro', NULL, '2020-06-11 23:07:28', NULL, NULL, NULL, NULL, NULL),
(52, 'E-0401053400', 'WILMER RUBEN', 'NARVAEZ MENA', 'wilor_nm@hotmail.com', '04242525266', 'manchas1979', NULL, '2020-06-12 12:13:46', NULL, NULL, NULL, NULL, NULL),
(53, 'V-14727634', 'Martin', 'Piñango', 'martin.pinango@gmail.com', '04166058176', '*Ma211081*', NULL, '2020-08-04 09:16:20', NULL, NULL, NULL, NULL, NULL),
(54, 'V-19401459', 'Manuel Alejandro', 'Espinal Altuve', 'espinaltuve@gmail.com', '04126041398', '*mezcla390', NULL, '2020-08-13 13:43:02', NULL, NULL, NULL, NULL, NULL),
(55, 'V-6122255', 'Armando ', 'Contreras', 'armandocontreras1963@gmail.com', '04143254246', 'jose2007', NULL, '2020-08-19 11:45:50', NULL, NULL, NULL, NULL, NULL),
(56, 'V-14327351', 'prueba', 'prueba', 'mmorgado@hotmail.com', '04122475643', '248605', NULL, '2020-08-19 22:44:47', NULL, NULL, NULL, NULL, NULL),
(57, 'V-10482993', 'OSCAR H', 'CUBILLAN M', 'cubillano@gmail.com', '04143203200', 'cub2llan2', NULL, '2020-08-26 14:38:25', NULL, NULL, NULL, NULL, NULL),
(58, 'J-303992722', 'Alicia M', 'Guzman M', 'alicia.guzman@willistowerswatson.com', '04242064664', 'parsaludag', NULL, '2020-08-27 15:36:35', 'PLAN ADMINISTRADO RONTARCA SALUD', NULL, NULL, NULL, NULL),
(59, 'V-7418756', 'Maria Lissett', 'Rodriguez de Andrade', 'Lissett.rodriguez@gmail.com', '04144665159', 'Sergio26.', NULL, '2020-09-07 21:53:32', NULL, NULL, NULL, NULL, NULL),
(62, 'V-20074571', 'Wilmer', 'Brito', 'wilmerbrit@gmail.com', '04122762302', 'wilmer23', NULL, '2020-09-18 13:23:26', NULL, NULL, NULL, NULL, NULL),
(63, 'V-6063567', 'Miguel', 'Rodriguez', 'miguelartu49@gmail.com', '04248856474', '11111', NULL, '2020-09-19 17:22:42', NULL, NULL, NULL, NULL, NULL),
(64, 'V-6670230', 'yoselys', 'pinto', 'yosman99@hotmail.com', '04263106785', 'abundancia*20', NULL, '2020-09-29 16:28:13', NULL, NULL, '@yosselyspinto', NULL, NULL),
(65, 'V-14274841', 'Francois', 'Touchie', 'francoistouchie@hotmail.com', '04167092423', '14274841', NULL, '2020-10-01 21:00:44', NULL, NULL, NULL, NULL, NULL),
(66, 'V-4319523', 'Rodolfo Eliecer', 'Matheus Urbina', 'rodolfo.matheus.urbina@gmail.com', '04143275268', 'Rmathepa43', NULL, '2020-10-02 10:43:05', NULL, NULL, NULL, NULL, NULL),
(67, 'V-18253735', 'JORGE', 'GOMEZ', 'jlgomez1487@gmail.com', '04241615115', 'Usb0913*', NULL, '2020-10-04 22:25:23', NULL, NULL, NULL, NULL, NULL),
(68, 'V-19132488', 'luis alfredo', 'motta carpio', 'lmotta1808@gmail.com', '04242802050', 'breakdown', NULL, '2020-10-05 11:35:13', NULL, NULL, NULL, NULL, NULL),
(69, 'V-15605252', 'ALEX', 'LLOVERA', 'erikamaria_h@hotmail.com', '04127363580', '180781', NULL, '2020-10-08 12:20:18', NULL, NULL, NULL, NULL, NULL),
(70, 'V-15119311', 'Edilma Luz', 'Giraldo Fernández', 'edilma.giraldo.fer@gmail.com', '04123286686', 'Pinche11', '1982-06-14', '2020-10-16 11:24:03', '', '', '', '', NULL),
(71, 'V-3670046', 'Freddy', 'Jeronimo', 'fjeronimoc@gmail.com', '04166071865', 'exiauto2020', NULL, '2020-10-16 14:56:06', NULL, NULL, NULL, NULL, NULL),
(72, 'V-3678046', 'Freddy', 'Jeronimo', 'freddy.jeronimo@gmail.com', '04166071865', 'exiauto2020', NULL, '2020-10-17 16:30:15', NULL, NULL, NULL, NULL, NULL),
(73, 'V-15026671', 'ARMANDO ', 'PINO ROJA', 'armando_pino69@hotmail.com', '04125590432', '15026671', '0000-00-00', '2020-10-29 10:25:39', '', '', '@armando.r.rojas.1', '', NULL),
(74, 'V-9420420', 'Vicente', 'Martinez Urbaneja', 'vimartinez@hotmail.com', '04142881173', 'K1k02507', NULL, '2020-10-30 14:38:20', NULL, NULL, NULL, NULL, NULL),
(75, 'V-6864186', 'Luis', 'Alfonzo', 'luisatilioalfonzo@gmail.com', '04242617620', 'sant1ago24', NULL, '2020-10-30 20:40:24', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE `vehiculos` (
  `id` int(11) NOT NULL,
  `id_modelo` int(11) DEFAULT NULL,
  `nom_veh` varchar(100) DEFAULT NULL,
  `ano_veh` int(11) DEFAULT NULL,
  `pre_veh` float DEFAULT NULL,
  `tra_veh` varchar(20) DEFAULT NULL,
  `est_veh` int(11) DEFAULT NULL,
  `imp_veh` varchar(1) DEFAULT NULL,
  `sto_veh` int(11) DEFAULT NULL,
  `des_veh` text DEFAULT NULL,
  `fle_veh` text DEFAULT NULL,
  `dis_veh` text DEFAULT NULL,
  `seg_veh` text DEFAULT NULL,
  `img1` text DEFAULT NULL,
  `img2` text DEFAULT NULL,
  `img3` text DEFAULT NULL,
  `img4` text DEFAULT NULL,
  `img5` text DEFAULT NULL,
  `doc` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`id`, `id_modelo`, `nom_veh`, `ano_veh`, `pre_veh`, `tra_veh`, `est_veh`, `imp_veh`, `sto_veh`, `des_veh`, `fle_veh`, `dis_veh`, `seg_veh`, `img1`, `img2`, `img3`, `img4`, `img5`, `doc`) VALUES
(5, 5, 'Yaris HB', 2019, 0, 'Manual', 1, '1', 0, 'DISEÑO AL SIGUIENTE NIVEL\r\n\r\nSu diseño va un paso más allá ofreciendo una presencia exterior distintiva, manteniendo la movilidad y agilidad necesaria para\r\nconducir con absoluta soltura.\r\n\r\nMAYOR AMPLITUD Y COMODIDAD\r\n\r\nCon su amplia carrocería, el Yaris Hatchback ofrece una comodidad excepcional. Su capacidad y amplitud son dos de las\r\ncaracterísticas más admirables de este modelo.', 'EFICIENCIA\r\n\r\nLa conducción del Yaris está por\r\nencima de su categoría, presentando\r\nestabilidad, seguridad y confort en la\r\ndirección. Equipado con dirección\r\nelectro-asistida, el vehículo tiene más\r\nfacilidad y suavidad en la conducción.\r\nSu motor 1.5L Dual VVT-i entrega\r\nrendimiento y eficiencia, apostando a\r\nla sincronía absoluta entre potencia y\r\nahorro de combustible', 'Las líneas modernas e imponentes\r\ntraen personalidad y deportividad\r\ncon la rejilla frontal tipo colmena.\r\nLos faros con un diseño\r\nsobresaliente tienen proyectores\r\nintegrados con máscara negra. En\r\ndefinitiva su diseño único hace del\r\nYaris un vehículo vanguardista y\r\nsofisticado.', 'MAYOR SEGURIDAD Y CONTROL\r\n\r\nConducir por la ciudad tiene sus riesgos y una carrocería más\r\nrígida proporciona mayor seguridad en caso de una colisión. Es\r\npor esto que hemos cambiado la forma en que las láminas de\r\nmetal se unen en la construcción de la carrocería. Además, para\r\nhacerlo aún más fuerte, aumentamos el número de puntos de\r\nsoldadura por 100.\r\n\r\nEquipado con airbags para conductor y\r\npasajero, el Yaris HB te proporciona\r\nseguridad. Además, está equipado con\r\ncontrol de estabilidad (VSC) y asistente de\r\nsubida (HAC) que proporciona comodidad\r\nen salidas con inclinación, impidiendo que\r\nel vehículo descienda por hasta tres\r\nsegundos para el cambio de pedal.', 'https://exiauto.com/static/img/files/2019_11_06_09_10_51yaris hb blanco frente.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_11_43_58yaris hb blanco lado.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_11_43_58yaris hb espalda blanco lado.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_11_43_58yaris hb espalda blanco.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_11_43_58yaris hb blanco frente.png', 'https://exiauto.com/static/img/files/2019_09_17_21_51_46formulario_solicitud_visa_DANIELA.pdf'),
(6, 5, 'Yaris SD', 2019, 0, 'Manual', 1, '1', 0, '+ ATRACTIVO + SEGURO + ESTABLE + CÓMODO', 'La conducción del Yaris está por\r\nencima de su categoría, presentando\r\nestabilidad, seguridad y confort en la\r\ndirección. Equipado con dirección\r\nelectro-asistida, el vehículo tiene más\r\nfacilidad y suavidad en la conducción.\r\nSu motor 1.5L Dual VVT-i entrega\r\nrendimiento y eficiencia, apostando a\r\nla sincronía absoluta entre potencia y\r\nahorro de combustible', 'Las líneas modernas e\r\nimponentes traen personalidad y\r\nsofisticación junto con la parrilla\r\nfrontal.\r\nLos faros con diseño destacado\r\nposeen proyectores integrados\r\ncon máscara negra.\r\n\r\nCon su amplia carrocería, el Yaris Sedan ofrece una comodidad excepcional. Su capacidad y amplitud son dos de\r\nlas características más admirables de este modelo.\r\nEl Yaris Sedan va un paso más lejos con un habitáculo atractivo y amplio, diseñado para brindar la máxima\r\ncomodidad y las mejores sensaciones a sus ocupantes.', 'MAYOR SEGURIDAD Y CONTROL\r\nConducir por la ciudad tiene sus riesgos y una carrocería más\r\nrígida proporciona mayor seguridad en caso de una colisión. Es\r\npor esto que hemos cambiado la forma en que las láminas de\r\nmetal se unen en la construcción de la carrocería. Además, para\r\nhacerlo aún más fuerte, aumentamos el número de puntos de\r\nsoldadura por 100.\r\n\r\nNew Yaris está equipado con la tecnología VSC la cual permite\r\nestabilizar el movimiento del vehículo, ayudando a que el\r\nconductor controle el derrape lateral.\r\nEquipado con hasta 7 airbags el Yaris Sedan\r\nte proporciona seguridad. Además, está\r\nequipado con control de estabilidad (VSC) y\r\nasistente de subida (HAC) que proporciona\r\ncomodidad en salidas con inclinación,\r\nimpidiendo que el vehículo descienda por\r\nhasta tres segundos para el cambio de\r\npedal.', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_12_39_46Picture2-min.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_12_39_46FRENTE LADO YARIS SUPER BLANCO.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_12_28_46FRENTE FRENTE YARIS BLANCO.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_12_39_46SUPER BLANCO YARIS ESPALDA LADO.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_12_28_46ESPALDA YARIS BLANCO.png', NULL),
(7, 6, 'Fortuner Importada', 2019, 0, 'Manual', 1, '1', 1, '', '', '', '', 'https://exiauto.com/static/img/files/2019_09_12_09_56_47frente fortuner importada-min.jpg', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_13_27_17faros delanteros fortuner importada.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_56_48interior puestos fortuner importada-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_56_48luces traseras fortuner importada-min.jpg', NULL, NULL),
(8, 7, 'Hiace Panel ', 2019, 0, 'Manual', 1, '1', 0, 'La Toyota Hiace diseñada para emprendedores que necesitan una máquina de carga perfecta para los objetivos de su negocio, sumamente versátil y de una alta funcionalidad.\r\n\r\n Déjate sorprender con la nueva Hiace, un espacio tan grande como tus sueños.', 'Toyota Hiace posee una excelente maniobrabilidad gracias a su reducido radio de giro (5.0 mts. para techo bajo, 6.2 mts. para techo alto).\r\n', '• Asientos delanteros reclinables tipo butaca con apoya cabeza \r\n• Faros de Halógeno \r\n• Tercer Stop (Tipo Led)\r\n• Múltiples compartimientos\r\n• Tapicería de tela Gris \r\n• Portavasos \r\n• Sistema de Audio AM/FM con entrada USB \r\n• Aire Acondicionado ', '• Cinturones de seguridad delanteros de 3 puntos (pasajero y co-piloto) \r\n• Cinturones de seguridad delantero de 2 puntos (central) \r\n• Estructura de carrocería rígida amortiguada contra impactos\r\n• Volante basculante y columna de dirección colapsable en caso de impacto\r\n• Barras de seguridad en las puertas', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11hiace blanca cerrada frente-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11hiace lado blanca cerrada}-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11panel beige and brown dos puestos-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11espalda desde afuera con vacia llena-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_12interior vacio dos puestos-min.PNG', NULL),
(9, 1, 'Corolla Nacional ', 2019, 0, 'Manual', 1, '0', 0, 'Corolla Más allá de la perfección\r\nLa experiencia de conducir un Toyota Corolla es inigualable, ya que posee el equilibrio perfecto entre potencia, maniobrabilidad y rendimiento, además de proveer un espacio cómodo, amplio, confortable para el conductor y toda su familia.\r\n\r\n\r\nPasión, innovación, elegancia, tecnología e imagen deportiva son elementos que marcan la creación del nuevo Toyota Corolla un vehículo nunca antes visto, una creación totalmente nueva producto de la ingeniería Toyota', '', 'Diseño vanguardista y dimensiones que proyectan mayor valor de mayor dimensiones:\r\nLargo: 4620mm (+80) / Ancho:1775 mm (+15) / Alto: 1475mm (-5) a mas largo y deportivo.\r\nDist. entre ejes: 2700mm (+100)= mayor espacio interior.\r\nParrilla incorporada a los faros = Elegancia y sensación de anchura.\r\nParrilla inferior en forma de trapezoide invertido (Deportivo).\r\nLíneas de corte más pronunciadas en los extremos de los parachoques (Guardafangos más anchos a estabilidad).\r\nLíneas de cortes pronunciadas en los laterales (largo y estabilidad).\r\nPlatina cromada que se entrelaza con los faros traseros = Presencia y elegancia.\r\nFaros tipo LED = Menor consumo, mayor iluminación y alcance.\r\nElementos aerodinámicos que ayudan a disminuir la fricción del viento y a la disminución del ruido.\r\nDiseño vanguardista y elegante con una fluidez horizontal (dinamismo).\r\nJuego de colores \"Claro - Oscuro\" (Amplitud y frescura).\r\nDist. entre ejes: 2700 (+100) = mayor espacio interior. Incrementa el espacio de rodillas en el asiento trasero en 75mm.\r\nMejora en los materiales simulando el cuero (Menos plástico y más nivel) con detalles de costura (Elegancia).\r\nAcabados en Piano Black y tipo metálicos (Lujo, Brillo y resistencia a las rayas).\r\nContinuidad de la parrilla en el faro.\r\nFaros tipo LED (GLi).\r\nNueva llave de encendido tipo navaja.\r\nVolante forrado en cuero con controles de audio y manos libres (GLi).\r\nConsola de techo (GLi).\r\nCámara de retroceso (GLi).\r\nAsiento con respaldar abatible (GLi).\r\nAmpliación del espacio en la maleta a pesar de tener cilindro de GNV.\r\nTres bolsas de aire (Conductor + Pasajero y Rodilla de conductor).\r\nNuevo radio de pantalla táctil.\r\nIpod, Teléfono, USB, SD Card, DVD, Bluetooth.\r\nFijación para sillas de bebé.', '', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_14_22_30FRENTE COROLLA.jpg', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_14_22_30LADO COROLLA.jpg', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_14_22_30LUCES COROLLA.jpg', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_14_22_30LUCES TRASERAS COROLLA.jpg', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_14_22_31MALETA COROLLA.jpg', NULL),
(10, 2, 'Camry', 2019, NULL, 'Automático', 1, '1', 0, 'Lujo y confort a simple vista\r\n', NULL, 'El nuevo Camry es la síntesis perfecta entre moderno, deportivo y elegante. El renovador frente realza su presencia a partir del diseño de las ópticas y de la parrilla inferior. Las llantas de 17” remarcan un perfil más deportivo y audaz.\r\n\r\nSu renovador interior ofrece asientos tapizados en cuero color negro y detalles similar a la madera. El audio con pantalla táctil de 8” y el display de información múltiple de 7” pueden controlarse desde el volante.', 'Además de tener una carrocería de seguridad para impactos con la clasificación global más alta, el Camry está repleto de funciones avanzadas en seguridad activa. Por si fuera poco, también está equipado con una serie de medidas de seguridad pasiva: airbags frontales, laterales, de cortina y de rodilla para proteger la integridad de los ocupantes ante la ocasión de un impacto.\r\n\r\nEl control de estabilidad (vsc) ayudará a mantener todo en orden ante situaciones adversas. El Camry cuenta con discos de 17” en las 4 ruedas, además de sistema de anti-bloqueo ABS y BA (BrakeAssist) que brindan la mayor seguridad y potencia de frenado.\r\n\r\n Airbags frontales, laterales, de cortina y de rodilla para conductor, para proteger la integridad de los ocupantes ante la ocasión de un impacto.', 'https://exiauto.com/static/img/files/2019_09_12_09_01_01camry calle.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_01_01camry frente.PNG', NULL, 'https://exiauto.com/static/img/files/2019_09_12_09_01_01espalda lado camry.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_01_02camry espalda.PNG', NULL),
(11, 7, 'Hiace Commuter', 2019, 0, 'Manual', 1, '1', 0, '', '', '', '', 'https://exiauto.com/static/img/files/2019_09_12_09_42_44hiace commuter frente-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_42_45hiace lado commuter-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_42_45hiace commuter espalda-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_42_45interior puestos-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_42_46panel 3 puestos-min.PNG', NULL),
(12, 6, 'Fortuner Nacional', 2019, 0, 'Manual', 1, '0', 0, '', '', '', '', 'https://exiauto.com/static/img/files/2019_09_12_09_49_54FRENTE FORTUNER NACIONAL-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_54LADO FORTUNER NACIONAL-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_55ESPALDA FORTUNER NACIONAL-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_55TABLERO FORTUNER-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_55ASIENTO FORTUNER-min.jpg', NULL),
(14, 10, 'Land Cruiser Prado VX', 2020, 0, 'Automático', 1, '1', 0, 'Con un aspecto fuerte y un alto nivel de calidad, durabilidad y confiabilidad, que alcanza los estándares más altos de diseño e innovación', 'Excelente maniobrabilidad', 'Diseño “modernidad inteligente” que da un carácter elegante y robusto, espacio interior más amplio', 'El radar detecta los vehículos que circulan en el siguiente carril. Cuando el vehículo ingresa al área del punto ciego, el indicador LED montado en el espejo de la puerta se enciende. En ese momento, si la luz de giro lateral parpadea, el indicador LED también parpadea para alertar al conductor.', 'https://exiauto.com/static/img/files/2019_10_02_11_24_39pradoprado.jpg', NULL, NULL, NULL, NULL, 'https://exiauto.com/static/img/files/2019_10_02_11_26_20Spec Sheet PRADO.pdf'),
(15, 6, 'Fortuner TRD Sportivo', 2020, 0, 'Automático', 1, '0', 0, 'La nueva Fortuner en sus dos versiones, 4x4 y 4x2, presenta la combinación perfecta entre elegancia, confort y un alto rendimiento capaz de operar con tranquilidad en condiciones extremas. La armonía del nuevo diseño presenta líneas redondeadas que proporcionan una sensación de fuerza y robustez con detalles deportivos, sin perder la elegancia y sofisticación de un vehículo de categoría superior.', 'Aire acondicionado automático.\r\nSistema de climatización automática digital.\r\n\r\n\r\nAire acondicionado trasero.\r\nSistema de enfriamiento trasero independiente para mayor confort en las dos filas de asientos posteriores.\r\n\r\n\r\nNumerosos espacios de almacenamiento.\r\nFortuner tiene diversos espacios de almacenamiento para colocar artículos que se deseen tener a la mano.\r\n\r\n\r\nConsola de techo con luces de lectura.\r\nLa consola superior entre los dos tapasoles está perfectamente adecuada para guardar los lentes de sol u otros artículos de tamaño similar. Igualmente posee doble lámparas de lectura para mayor confort del cliente.', 'Tablero central y paneles.\r\nDetalles internos cromados y apliques tipo madera oscura.\r\n\r\n\r\nInnovador panel de instrumento optitrón.\r\nEl panel de instrumentos tipo optitron permite al conductor tener una fácil lectura de cada uno de los componentes del tablero, permitiendo una conducción más segura.\r\n\r\n\r\nVolantes con controles de audio.\r\nLos controles de audio ubicados en el volante le permiten ajustar las diferentes funciones del sistema de audio (volúmen, estaciones, canciones y modo de reproducción) sin perder la visión en el camino, permitiendo su manejo seguro.\r\n\r\n\r\nTapicería de cuero.\r\n', 'Sistema inmobilizer\r\nPara mayor seguridad, la Fortuner está equipada con un sistema que bloquea el encendido.\r\n\r\n\r\nABS\r\nSistema de frenos antibloqueo-ABS\r\n\r\n\r\nDoble bolsa de aire\r\nDoble bolsa de aire para conductor y pasajero.\r\n\r\n\r\nCarrocería de seguridad\r\nLa carrocería de seguridad contra choques abarca un habitáculo de alta integridad, con zonas de absorción de impacto delantera y traseras, que minimizan la transmisión del impacto hacia los pasajeros.\r\n\r\n\r\nAlarma antirrobo.\r\nLa Fortuner tiene un sistema de seguridad que avisa en caso de un robo o el intento del mismo.', 'https://exiauto.com/static/img/files/2019_10_17_14_10_18fortuner trd sportivo.jpg', NULL, NULL, NULL, NULL, NULL),
(16, 8, 'Hilux Gr Sport', 2020, 0, 'Automático', 1, '1', 0, 'Todo lo que aprendimos de las competencias más exigentes llevado a una Pick-Up', 'Robusta y deportiva, con una parrilla imponente que destaca el emblema Toyota, barra y estribos de diseño exclusivo y llantas de aleación específicas.', 'Destacan los colores negro y rojo. Las butacas son de cuero natural ecológico, y todas vienen con un número único que las identifica. La nueva GR-Sport respira las carreras incluso desde el botón de arranque. ', 'Todas las versiones vienen equipadas en la parte delantera con una cámara frontal Dashcam que registra tus momentos en Full HD, y viene con GPS y WiFi incorporado.', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-3.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-4.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-5.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-6.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_12gr sport-55.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_12Hilux GR Sport - Specs.pdf');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuario` (`usuario`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indices de la tabla `agentes`
--
ALTER TABLE `agentes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cod_age` (`cod_age`);

--
-- Indices de la tabla `categorias_repuestos`
--
ALTER TABLE `categorias_repuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_vehiculo` (`id_vehiculo`),
  ADD KEY `id_agente` (`id_agente`),
  ADD KEY `id_falla` (`id_falla`),
  ADD KEY `id_kilometros` (`id_kilometros`);

--
-- Indices de la tabla `comentarios_reclamos`
--
ALTER TABLE `comentarios_reclamos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_reclamo` (`id_reclamo`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nom_dep` (`nom_dep`);

--
-- Indices de la tabla `fallas`
--
ALTER TABLE `fallas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `kilometrajes`
--
ALTER TABLE `kilometrajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `misvehiculos`
--
ALTER TABLE `misvehiculos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `placa` (`placa`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `misvehiculos_ibfk_2` (`id_modelo`);

--
-- Indices de la tabla `modelos`
--
ALTER TABLE `modelos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modelos_citas`
--
ALTER TABLE `modelos_citas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_departamento` (`id_departamento`);

--
-- Indices de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- Indices de la tabla `solicitud_repuestos`
--
ALTER TABLE `solicitud_repuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ced_usu` (`ced_usu`),
  ADD UNIQUE KEY `cor_usu` (`cor_usu`);

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_modelo` (`id_modelo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `agentes`
--
ALTER TABLE `agentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `categorias_repuestos`
--
ALTER TABLE `categorias_repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT de la tabla `comentarios_reclamos`
--
ALTER TABLE `comentarios_reclamos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `fallas`
--
ALTER TABLE `fallas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT de la tabla `kilometrajes`
--
ALTER TABLE `kilometrajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;

--
-- AUTO_INCREMENT de la tabla `misvehiculos`
--
ALTER TABLE `misvehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `modelos`
--
ALTER TABLE `modelos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `modelos_citas`
--
ALTER TABLE `modelos_citas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `solicitud_repuestos`
--
ALTER TABLE `solicitud_repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `citas_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `citas_ibfk_2` FOREIGN KEY (`id_vehiculo`) REFERENCES `misvehiculos` (`id`),
  ADD CONSTRAINT `citas_ibfk_3` FOREIGN KEY (`id_agente`) REFERENCES `agentes` (`id`),
  ADD CONSTRAINT `citas_ibfk_4` FOREIGN KEY (`id_falla`) REFERENCES `fallas` (`id`),
  ADD CONSTRAINT `citas_ibfk_5` FOREIGN KEY (`id_kilometros`) REFERENCES `kilometrajes` (`id`);

--
-- Filtros para la tabla `comentarios_reclamos`
--
ALTER TABLE `comentarios_reclamos`
  ADD CONSTRAINT `comentarios_reclamos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `comentarios_reclamos_ibfk_2` FOREIGN KEY (`id_admin`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `comentarios_reclamos_ibfk_3` FOREIGN KEY (`id_reclamo`) REFERENCES `reclamos` (`id`);

--
-- Filtros para la tabla `misvehiculos`
--
ALTER TABLE `misvehiculos`
  ADD CONSTRAINT `misvehiculos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `misvehiculos_ibfk_2` FOREIGN KEY (`id_modelo`) REFERENCES `modelos_citas` (`id`);

--
-- Filtros para la tabla `reclamos`
--
ALTER TABLE `reclamos`
  ADD CONSTRAINT `reclamos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `reclamos_ibfk_2` FOREIGN KEY (`id_departamento`) REFERENCES `departamentos` (`id`);

--
-- Filtros para la tabla `repuestos`
--
ALTER TABLE `repuestos`
  ADD CONSTRAINT `repuestos_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorias_repuestos` (`id`);

--
-- Filtros para la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD CONSTRAINT `vehiculos_ibfk_1` FOREIGN KEY (`id_modelo`) REFERENCES `modelos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
